# coding: utf-8
import glob
import numpy as np
np.set_printoptions(precision=3,suppress=True)

from musa.musa import Musa
m = Musa()

mcifs=glob.glob("./cifs/*.mcif")


# Load structure file
m.load_cif('./cifs/4107570.cif')

# The symmetry saved in the cif file is automatically parsed.
# However, if you don't have a cif file, you can always use the spg
# library to identify the symmetry. This is done with the following 
# command.
m.sym_search()

# We set the knwon position of the muon
m.muon_set_frac([0.75,0.75,0.635])

# We use symmetry to identify the equivalent positions.
#  1e-3 is the minimum "distance" betweed two sites in scaled coordinates
#  before they are counted as the same site.
m.muon_find_equiv(1.e-3)

# Load magnetic structures for mcif file.
#
for mcif in mcifs:
    # This function tries to identify the propagation vector and the FC
    m.mago_load_from_mcif(mcif)
    # do the summation
    res=m.locfield('s',[100,100,50],200)
    
    print('Structure in file: '+mcif)
    for e in res:
        print(e.T*0.66, "Norm {: 0.4f}".format(np.linalg.norm(e.T*0.66)))    

print ('\n Now with all the symmetry equivalent sites for real.\n\n')
for mcif in mcifs:
# Load all maximally symmetric structures (obtained from bilbao)
#  for propagation vector (1/2,1/2,0)
#  Crystal structure is included and is parsed too.
    m.load_mcif(mcif)
# finds cell symmetries
#  these are inspected from the structure rather than parsed from the cif.
#  The reason for doing this is that structure may be read from files
#  without symmetry informations.
    m.sym_search()
# Sets muon position in fractional coordinates
    m.muon_set_frac('0.375  0.375 0.635')
# Find crystalographically equivalent muon sites. Uses 3 decimal rounding
#  to check if two positions are equivalent.
    m.muon_find_equiv(3)
    res=m.locfield('s',[100,100,50],200)
    print('Structure in file: '+mcif)
    for e in res:
        print(e.T*0.66, "Norm {: 0.4f}".format(np.linalg.norm(e.T*0.66)))

print('\n --> According to PhysRevB 80 094524, the local field at the muon site is about 1700 G')
print('\n --> So long range order is either bcs_file_22902c.mcif or ./bcs_file_22902d.mcif \n')
