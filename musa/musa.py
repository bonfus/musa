import __main__ as main

from musa.settings import config
from musa.core.nprint import cstring
from musa.core.nprint  import nprint, nprintmsg
from musa.core.spg  import Spacegroup

from musa.modules.xsf.xsf import XSF
from musa.modules.cif.cif import CIF
from musa.modules.printer import Print
from musa.modules.muon import Muon
from musa.modules.ms import MS
from musa.modules.dft_grid import DFTGrid
from musa.modules.clfc.clfc import LFC

from musa.modules.symsearch import SYMSEARCH

    
from musa.modules.supercell import Supercell



class Musa(Print,XSF,CIF,Muon,MS,LFC,SYMSEARCH,Supercell,DFTGrid):
    
    def __init__(self):
        
        self._interactive = not hasattr(main, '__file__')
        
        if (self._interactive):
	        nprint('Interactive musa! Welcome.')
        
        self._cell = None
        self._supercell = None
        self._magdefs = []
        self._muon = []
        self._sym = None
        
        Print.__init__(self)
        XSF.__init__(self)
        CIF.__init__(self)
        Muon.__init__(self)
        MS.__init__(self)
        LFC.__init__(self)
        SYMSEARCH.__init__(self)
        Supercell.__init__(self)
        
    def __repr__(self):
        status = "Musa status: \n\n"
        
        # Check crystal structure
        status += " Crystal structure:".ljust(30)
        if self._cell is None:
            status += cstring('No','warn')
        else:
            status += cstring('Yes','ok')
        status += '\n'
        
        # Check supercell structure
        status += " Supercell structure:".ljust(30)
        if self._supercell is None:
            status += cstring('No','warn')
        else:
            status += cstring('Yes','ok')
        status += '\n'

        # Check magnetic structure
        status += " Magnetic structure:".ljust(30)
        if not self._magdefs:
            status += cstring('No','warn')
        else:
            status += cstring('Yes','ok')
        status += '\n'

        # Check muon position
        status += " Muon position(s):".ljust(30)
        if not self._muon:
            status += cstring('No','warn')
        else:
            status += cstring(str(len(self._muon))+' site(s)','ok')
        status += '\n'

        # Check symmetry
        status += " Symmetry data:".ljust(30)
        if not self._sym:
            status += cstring('No','warn')
        else:
            status += cstring('Yes','ok')
        status += '\n'

        return status


    def _reset(self,cell=True,supercell=True,magdefs=True,muon=True,sym=True):
        if cell:
            self._cell = None
        if supercell:
            self._supercell = None
        if magdefs:
            self._magdefs = []
            self._selected_order = -1
        if muon:
            self._muon = []
        if sym:
            self._sym = None

    def _check_sym(self):
        if self._sym is None:
            nprintmsg('esym')
            return False
        if not (isinstance(self._sym,Spacegroup)):
            nprintmsg('esym')
            return False
			
        return True

    def _check_lattice(self):
        if self._cell is None:
            nprintmsg('ecrystal')
            return False
        return True
        
    def _check_magdefs(self):
        if not self._magdefs:
            nprintmsg('emag')
            return False
        return True
        
    def _check_supercell(self):
        if self._supercell is None:
            nprintmsg('esupcell')
            return False
        return True
        
    def _check_muon(self):
        if self._muon:
            return True           
        nprintmsg('emuon')
        return False                        
