# Copyright (C) 2011 Atsushi Togo
# All rights reserved.
#
# This file is part of phonopy.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
#
# * Neither the name of the phonopy project nor the names of its
#   contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import numpy as np
from musa.core.atoms import Atoms


def get_supercell(unitcell, supercell_matrix, symprec = 1e-5, FC=None, K=np.array([0,0,0]), PHI=None, chk_for_overlap = False, map_to_unit_cell = False):
    """Build supercell from supercell matrix
    In this function, unit cell is considered 
    [1,0,0]
    [0,1,0]
    [0,0,1].
    Supercell matrix is given by relative ratio, e.g,
    [-1, 1, 1]
    [ 1,-1, 1]  is for FCC from simple cubic.
    [ 1, 1,-1]
    In this case multiplicities of surrounding simple lattice are [2,2,2].

    First, create supercell with surrounding simple lattice.
    Second, trim the surrounding supercell with the target lattice.
    """
    
    mat = np.array(supercell_matrix)
    frame = get_surrounding_frame( mat )

    surrounding_cell = get_simple_supercell( frame, unitcell, FC, K , PHI)

    # Trim the simple supercell by the supercell matrix
    trim_frame = np.array( [mat[0] / float(frame[0]),
                            mat[1] / float(frame[1]),
                            mat[2] / float(frame[2])] )
    
    if chk_for_overlap:
        supercell, mapping = trim_cell( trim_frame,
                                            surrounding_cell,
                                            symprec )
    else:
        supercell, mapping = trim_cell_wo( trim_frame,
                                            surrounding_cell,
                                            symprec )
    multi = np.prod( frame )
    s2u_map = []
    if map_to_unit_cell:
        for i in range( supercell.get_number_of_atoms() ):
            s2u_map.append( mapping.index( ( mapping[i] // multi ) * multi ) )

    return Supercell( supercell, s2u_map )

def get_simple_supercell( multi, unitcell, FC, K, PHI ):
    # Scaled positions within the frame, i.e., create a supercell that
    # is made simply to multiply the input cell.
    positions = unitcell.get_scaled_positions()
    numbers = unitcell.get_atomic_numbers()
    masses = unitcell.get_masses()
    lattice = unitcell.get_cell()
    
    positions_multi = []
    numbers_multi = []
    masses_multi = []
    if FC is None:
        magmoms_multi = None
    else:
        magmoms_multi = []
        fc_multi = []
    for l, pos in enumerate(positions):
        if numbers[l] == 0:    #  Check again if muon in there!
            raise RuntimeError #  This shuld never happen!
            continue
        for k in range(multi[2]):
            for j in range(multi[1]):
                for i in range(multi[0]):
                    positions_multi.append([ (pos[0] + i) / multi[0],
                                             (pos[1] + j) / multi[1],
                                             (pos[2] + k) / multi[2] ])
                    numbers_multi.append(numbers[l])
                    masses_multi.append(masses[l])
                    if not FC is None:

                        
                        c = np.cos ( 2.0*np.pi * np.dot(K,[float(i),float(j),float(k)]) + PHI[l] )
                        s = np.sin ( 2.0*np.pi * np.dot(K,[float(i),float(j),float(k)]) + PHI[l] );
                        
                        sk = np.real(FC[l])
                        isk = np.imag(FC[l])
                        
                        m = np.zeros(3)
                        m = c*sk + s*isk
                        
                        #print "Norma: " , np.linalg.norm(m)
                        magmoms_multi.append(m)   
                        fc_multi.append(sk)                         
                            



    return Atoms(numbers = numbers_multi,
                 masses = masses_multi,
                 magmoms = magmoms_multi,
                 scaled_positions = positions_multi,
                 cell = np.dot( np.diag( multi ), lattice ),
                 pbc=True)




def get_diluted_supercell(unitcell, supercell_matrix, dil_species, dil_factor, symprec = 1e-5, K=np.array([0,0,0]), chk_for_overlap = False, map_to_unit_cell = False):
    """Build supercell from supercell matrix
    In this function, unit cell is considered 
    [1,0,0]
    [0,1,0]
    [0,0,1].
    Supercell matrix is given by relative ratio, e.g,
    [-1, 1, 1]
    [ 1,-1, 1]  is for FCC from simple cubic.
    [ 1, 1,-1]
    In this case multiplicities of surrounding simple lattice are [2,2,2].

    dil species in int numer of atomic number
    dil factor: float, dilution factor from 0 to 1

    First, create supercell with surrounding simple lattice.
    Second, trim the surrounding supercell with the target lattice.
    """
    
    mat = np.array(supercell_matrix)
    frame = get_surrounding_frame( mat )

    
    surrounding_cell, eff_dil_factor = get_simple_diluted_supercell( frame, unitcell, K, dil_species, dil_factor )

    # Trim the simple supercell by the supercell matrix
    trim_frame = np.array( [mat[0] / float(frame[0]),
                            mat[1] / float(frame[1]),
                            mat[2] / float(frame[2])] )
    
    if chk_for_overlap:
        
        supercell, mapping = trim_cell( trim_frame,
                                            surrounding_cell,
                                            symprec )
    else:
        supercell, mapping = trim_cell_wo( trim_frame,
                                            surrounding_cell,
                                            symprec )
    multi = np.prod( frame )
    s2u_map = []
    if map_to_unit_cell:
        for i in range( supercell.get_number_of_atoms() ):
            s2u_map.append( mapping.index( ( mapping[i] // multi ) * multi ) )

    return (Supercell( supercell, s2u_map ), eff_dil_factor)

def get_simple_diluted_supercell( multi, unitcell, K, dil_species, dil_factor ):
    # Scaled positions within the frame, i.e., create a supercell that
    # is made simply to multiply the input cell.
    positions = unitcell.get_scaled_positions()
    numbers = unitcell.get_atomic_numbers()
    masses = unitcell.get_masses()
    magmoms = unitcell.get_magnetic_moments()
    lattice = unitcell.get_cell()
    
    positions_multi = []
    numbers_multi = []
    masses_multi = []
    if magmoms == None:
        magmoms_multi = None
    else:
        magmoms_multi = []
        
    if type(dil_species) is int:
        dil_species = [dil_species]
    
    dil_species_count = 0
    dil_species_rem = 0
    
    for l, pos in enumerate(positions):
        if numbers[l] == 0:    #  Check again if muon in there!
            raise RuntimeError #  This should never happen!
            continue
        for k in range(multi[2]):
            for j in range(multi[1]):
                for i in range(multi[0]):
                    if numbers[l] in dil_species:
                        dil_species_count += 1
                        if np.random.random() < dil_factor:
                            dil_species_rem += 1
                            continue
                        
                    positions_multi.append([ (pos[0] + i) / multi[0],
                                             (pos[1] + j) / multi[1],
                                             (pos[2] + k) / multi[2] ])
                    numbers_multi.append(numbers[l])
                    masses_multi.append(masses[l])
                    if not magmoms == None:
                        m = np.real_if_close(np.exp(-2j*np.pi*np.dot(K,[i,j,k])))
                        magmoms_multi.append(magmoms[l]*m)

    return (Atoms(numbers = numbers_multi,
                 masses = masses_multi,
                 magmoms = magmoms_multi,
                 scaled_positions = positions_multi,
                 cell = np.dot( np.diag( multi ), lattice ),
                 pbc=True), float(dil_species_rem)/float(dil_species_count))










def get_surrounding_frame(supercell_matrix):
    # Build a frame surrounding supercell lattice
    # For example,
    #  [2,0,0]
    #  [0,2,0] is the frame for FCC from simple cubic.
    #  [0,0,2]
    m = np.array( supercell_matrix )
    axes = np.array( [ [ 0, 0, 0],
                       m[:,0],
                       m[:,1],
                       m[:,2],
                       m[:,1] + m[:,2],
                       m[:,2] + m[:,0],
                       m[:,0] + m[:,1],
                       m[:,0] + m[:,1] + m[:,2] ] )
    frame = [ max( axes[:,i] ) - min( axes[:,i] ) for i in (0,1,2) ]
    return frame


def trim_cell_wo(relative_axes, cell, symprec):
    # relative_axes: relative axes to supercell axes
    # Trim positions outside relative axes

    positions = cell.get_scaled_positions()
    numbers = cell.get_atomic_numbers()
    masses = cell.get_masses()
    magmoms = cell.get_magnetic_moments()    
    lattice = cell.get_cell()
    trimed_lattice = np.dot( relative_axes.T, lattice )


    trimed_positions = []
    trimed_numbers = []
    trimed_masses = []
    if magmoms is None:
        trimed_magmoms = None
    else:
        trimed_magmoms = []
    atom_map = []
    for i, pos in enumerate(positions):
        # tmp_pos is the position with respect to the relative axes
        tmp_pos = np.dot(pos, np.linalg.inv(relative_axes).T)
        # Positions are folded into trimed cell and
        # overlap positions are removed.
        trimed_positions.append(tmp_pos - np.floor(tmp_pos))
        trimed_numbers.append(numbers[i])
        trimed_masses.append(masses[i])
        if not magmoms is None:
            trimed_magmoms.append(magmoms[i])
        atom_map.append(i)


    trimed_cell = Atoms( numbers = trimed_numbers,
                         masses = trimed_masses,
                         magmoms = trimed_magmoms,
                         scaled_positions = trimed_positions,
                         cell = trimed_lattice,
                         pbc=True)

    return trimed_cell, atom_map


def trim_cell(relative_axes, cell, symprec):
    # relative_axes: relative axes to supercell axes
    # Trim positions outside relative axes

    positions = cell.get_scaled_positions()
    numbers = cell.get_atomic_numbers()
    masses = cell.get_masses()
    magmoms = cell.get_magnetic_moments()    
    lattice = cell.get_cell()
    trimed_lattice = np.dot( relative_axes.T, lattice )


    trimed_positions = []
    trimed_numbers = []
    trimed_masses = []
    if magmoms == None:
        trimed_magmoms = None
    else:
        trimed_magmoms = []
    atom_map = []
    for i, pos in enumerate(positions):
        is_overlap = False
        # tmp_pos is the position with respect to the relative axes
        tmp_pos = np.dot(pos, np.linalg.inv(relative_axes).T)
        # Positions are folded into trimed cell and
        # overlap positions are removed.
        for t_pos in trimed_positions:
            diff = t_pos - tmp_pos
            diff -= diff.round()
            if np.linalg.norm( np.dot( diff, trimed_lattice ) ) < symprec:
                is_overlap = True
                break

        if not is_overlap:
            trimed_positions.append(tmp_pos - np.floor(tmp_pos))
            trimed_numbers.append(numbers[i])
            trimed_masses.append(masses[i])
            if not magmoms == None:
                trimed_magmoms.append(magmoms[i])
            atom_map.append(i)
        else:
            print ("Overlap!")

    trimed_cell = Atoms( numbers = trimed_numbers,
                         masses = trimed_masses,
                         magmoms = trimed_magmoms,
                         scaled_positions = trimed_positions,
                         cell = trimed_lattice,
                         pbc=True)

    return trimed_cell, atom_map

def print_cell(cell, mapping=None):
    symbols = cell.get_chemical_symbols()
    masses = cell.get_masses()
    magmoms = cell.get_magnetic_moments()
    lattice = cell.get_cell()
    print ("Lattice vectors:")
    print ("  a %20.15f %20.15f %20.15f" % tuple( lattice[0] ))
    print ("  b %20.15f %20.15f %20.15f" % tuple( lattice[1] ))
    print ("  c %20.15f %20.15f %20.15f" % tuple( lattice[2] ))
    print ("Atomic positions (fractional):")
    for i, v in enumerate(cell.get_scaled_positions()):
        if magmoms == None:
            print ("%5d %-2s%18.14f%18.14f%18.14f %7.3f" % \
                (i+1, symbols[i], v[0], v[1], v[2], masses[i]))
        else:
            print ("%5d %-2s%18.14f%18.14f%18.14f %7.3f  %s" % \
                (i+1, symbols[i], v[0], v[1], v[2], masses[i], ', '.join('%.3f' % val for val in magmoms[i])))
        # print 
        if mapping == None:
            print()
        else:
            print (">", mapping[i]+1)


class Supercell(Atoms):
    def __init__( self, supercell, supercell_to_unitcell_map ):
        Atoms.__init__( self,
                        numbers = supercell.get_atomic_numbers(),
                        masses = supercell.get_masses(),
                        magmoms = supercell.get_magnetic_moments(),
                        scaled_positions = supercell.get_scaled_positions(),
                        cell = supercell.get_cell(),
                        pbc = True )
        self.s2u_map = supercell_to_unitcell_map

    def get_supercell_to_unitcell_map( self ):
        return self.s2u_map

    def add_dislocations(self, disloc_cell, ijk):
        print (self.s2u_map)
        for pos in disloc_cell.get_scaled_positions():
            print (pos)

class Primitive(Atoms):
    def __init__(self, supercell, primitive_frame, symprec = 1e-5):
        """
        primitive_frame ( 3x3 matrix ):
        Primitive lattice is given by
           np.dot( primitive_frame.T, supercell.get_cell())
        """
        self.frame = np.array(primitive_frame)
        self.symprec = symprec
        self.p2s_map = None
        self.s2p_map = None
        self.p2p_map = None
        self.__primitive_cell( supercell )
        self.__supercell_to_primitive_map( supercell.get_scaled_positions() )
        self.__primitive_to_primitive_map()

    def __primitive_cell(self, supercell):
        trimed_cell, p2s_map = trim_cell( self.frame,
                                          supercell,
                                          self.symprec )
        Atoms.__init__( self,
                        numbers = trimed_cell.get_atomic_numbers(),
                        masses = trimed_cell.get_masses(),
                        magmoms = trimed_cell.get_magnetic_moments(),
                        scaled_positions = trimed_cell.get_scaled_positions(),
                        cell = trimed_cell.get_cell(),
                        pbc = True )

        self.p2s_map = p2s_map

    def get_primitive_to_supercell_map(self):
        return self.p2s_map

    def get_frame(self):
        return self.frame

    def __supercell_to_primitive_map(self, pos):
        inv_F = np.linalg.inv(self.frame)
        s2p_map = []
        for i in range(pos.shape[0]):
            s_pos = np.dot( pos[i], inv_F.T )
            for j in self.p2s_map:
                p_pos = np.dot( pos[j], inv_F.T )
                diff = p_pos - s_pos
                diff -= diff.round()
                if ( abs(diff) < self.symprec ).all():
                    s2p_map.append(j)
                    break
        self.s2p_map = s2p_map

    def get_supercell_to_primitive_map(self):
        return self.s2p_map

    def __primitive_to_primitive_map(self):
        """
        Mapping table from supercell index to primitive index
        in primitive cell
        """
        self.p2p_map = dict([ ( j, i ) for i, j in enumerate( self.p2s_map ) ])

    def get_primitive_to_primitive_map(self):
        return self.p2p_map



#
# Get distance between a pair of atoms
#
def get_distance(atoms, a0, a1, tolerance=1e-5):
    """
    Return the shortest distance between a pair of atoms in PBC
    """
    reduced_bases = get_reduced_bases( atoms.get_cell(), tolerance)
    scaled_pos = np.dot( atoms.get_positions(), np.linalg.inv(reduced_bases) )
    # move scaled atomic positions into -0.5 < r <= 0.5
    for pos in scaled_pos:
        pos -= pos.round()

    # Look for the shortest one in surrounded 3x3x3 cells 
    distances = []
    for i in (-1, 0, 1):
        for j in (-1, 0, 1):
            for k in (-1, 0, 1):
                distances.append( np.linalg.norm(
                        np.dot(scaled_pos[a0] - scaled_pos[a1] + np.array([i,j,k]),
                               reduced_bases) ) )
    return min(distances)

def get_distance_with_center( atoms, center, atom_num, tolerance=1e-5 ):
    """
    Return the shortest distance to atom from specified position
    """
    reduced_bases = get_reduced_bases( atoms.get_cell(), tolerance)
    scaled_pos = np.dot( atoms.get_positions(), np.linalg.inv(reduced_bases) )
    # move scaled atomic positions into -0.5 < r <= 0.5
    #for pos in scaled_pos:
    #    pos -= pos.round()
    #print pos
    #print scaled_pos
    # Look for the shortest one in surrounded 3x3x3 cells 
    distances = []
    for i in (-1, 0, 1):
        for j in (-1, 0, 1):
            for k in (-1, 0, 1):
                distances.append( np.linalg.norm(
                        np.dot(scaled_pos[atom_num] - center + np.array([i,j,k]),
                               reduced_bases) ) )
    return min(distances)

def get_atom_distance( scaled_pos, center, cell, tolerance=1e-5 ):
    """
    Return the shortest distance to atom from specified position
    """
    reduced_bases = get_reduced_bases(cell, tolerance)
    # move scaled atomic positions into -0.5 < r <= 0.5
    #for pos in scaled_pos:
    #    pos -= pos.round()
    #print pos
    #print scaled_pos
    # Look for the shortest one in surrounded 3x3x3 cells 
    distances = []
    for i in (-1, 0, 1):
        for j in (-1, 0, 1):
            for k in (-1, 0, 1):
                distances.append( np.linalg.norm(
                        np.dot(scaled_pos - center + np.array([i,j,k]),
                               reduced_bases) ) )
    return min(distances)    

def get_atom_vec( scaled_pos, center, cell, tolerance=1e-5 ):
    """
    Return the shortest distance to atom from specified position
    """
    reduced_bases = get_reduced_bases(cell, tolerance)
    # move scaled atomic positions into -0.5 < r <= 0.5
    #for pos in scaled_pos:
    #    pos -= pos.round()
    #print pos
    #print scaled_pos
    # Look for the shortest one in surrounded 3x3x3 cells 
    distances = []
    for i in (-1, 0, 1):
        for j in (-1, 0, 1):
            for k in (-1, 0, 1):
                a = np.dot(scaled_pos - center + np.array([i,j,k]),
                               reduced_bases)
                distances.append(
                        np.dot(scaled_pos - center + np.array([i,j,k]),
                               reduced_bases) )
                print (np.linalg.norm(a))
                print ("i j k: %i %i %i" % (i,j,k))
    #print distances
    #print [np.linalg.norm(item) for item in distances]
    return min(distances, key = lambda item: (np.linalg.norm(item)))     


#
# Delaunay reduction
#    
def get_reduced_bases(cell, tolerance=1e-5):
    """
    This is an implementation of Delaunay reduction.
    Some information is found in International table.
    """
    return get_Delaunay_reduction(cell, tolerance)

def get_Delaunay_reduction(lattice, tolerance):
    extended_bases = np.zeros((4,3), dtype=float)
    extended_bases[:3,:] = lattice
    extended_bases[3] = -np.sum(lattice, axis=0)

    for i in range(100):
        if reduce_bases(extended_bases, tolerance):
            break
    if i == 99:
        print ("Delaunary reduction is failed.")

    shortest = get_shortest_bases_from_extented_bases(extended_bases, tolerance)

    return shortest

def reduce_bases(extended_bases, tolerance):
    metric = np.dot(extended_bases, extended_bases.transpose())
    for i in range(4):
        for j in range(i+1, 4):
            if metric[i][j] > tolerance:
                for k in range(4):
                    if (not k == i) and (not k == j):
                        extended_bases[k] += extended_bases[i]
                extended_bases[i] = -extended_bases[i]
                extended_bases[j] = extended_bases[j]
                return False

    # Reduction is completed.
    # All non diagonal elements of metric tensor is negative.
    return True

def get_shortest_bases_from_extented_bases(extended_bases, tolerance):

    def mycmp(x, y):
        return cmp(np.vdot(x,x), np.vdot(y,y))

    basis = np.zeros((7,3), dtype=float)
    basis[:4] = extended_bases
    basis[4]  = extended_bases[0] + extended_bases[1]
    basis[5]  = extended_bases[1] + extended_bases[2]
    basis[6]  = extended_bases[2] + extended_bases[0]
    # Sort bases by the lengthes (shorter is earlier)
    basis = sorted(basis, cmp=mycmp)
    
    # Choose shortest and linearly independent three bases
    # This algorithm may not be perfect.
    for i in range(7):
        for j in range(i+1, 7):
            for k in range(j+1, 7):
                if abs(np.linalg.det([basis[i],basis[j],basis[k]])) > tolerance:
                    return np.array([basis[i],basis[j],basis[k]])

    print ("Delaunary reduction is failed.")
    return basis[:3]

#
# Other tiny tools
#    
def get_angles( lattice ):
    a, b, c = get_cell_parameters( lattice )
    alpha = np.arccos(np.vdot(lattice[1], lattice[2]) / b / c ) / np.pi * 180
    beta  = np.arccos(np.vdot(lattice[2], lattice[0]) / c / a ) / np.pi * 180
    gamma = np.arccos(np.vdot(lattice[0], lattice[1]) / a / b ) / np.pi * 180
    return alpha, beta, gamma

def get_cell_parameters( lattice ):
    return np.sqrt( np.dot ( lattice, lattice.transpose() ).diagonal() )

def get_cell_matrix( a, b, c, alpha, beta, gamma ):
    # These follow 'matrix_lattice_init' in matrix.c of GDIS
    alpha *= np.pi / 180
    beta *= np.pi / 180
    gamma *= np.pi / 180
    a1 = a
    a2 = 0.0
    a3 = 0.0
    b1 = np.cos( gamma )
    b2 = np.sin( gamma )
    b3 = 0.0
    c1 = np.cos( beta )
    c2 = ( 2 * np.cos( alpha ) + b1**2 + b2**2 - 2 * b1 * c1 - 1 ) / ( 2 * b2 )
    c3 = np.sqrt( 1 - c1**2 - c2**2 )
    lattice = np.zeros( ( 3, 3 ), dtype=float )
    lattice[ 0, 0 ] = a
    lattice[ 1 ] = np.array( [ b1, b2, b3 ] ) * b
    lattice[ 2 ] = np.array( [ c1, c2, c3 ] ) * c
    return lattice

def get_reciprocal_lattice( lattice ):
    #return reciprocal lattice vectors
    
    #Construct the metric tensor
    g = gtensor(lattice)

    # inverse of metric tensor
    gi = 2 * np.pi * np.linalg.inv(g)
    r_lattice = np.dot(gi,lattice)
    #V = np.sqrt(np.linalg.det(g))
    #print V
    return r_lattice


def gtensor(lattice):
    "calculates the metric tensor of a lattice"
    g=np.zeros((3, 3), 'd')
    #print 'shape ', g.shape
    a, b, c = get_cell_parameters(lattice)
    alpha, beta, gamma = get_angles(lattice)

    alpha *= np.pi / 180
    beta *= np.pi / 180
    gamma *= np.pi / 180
    
    g[0, 0]=a**2;
    g[0, 1]=a*b*np.cos(gamma)
    g[0, 2]=a*c*np.cos(beta)

    g[1, 0]=g[0, 1]
    g[1, 1]=b**2
    g[1, 2]=c*b*np.cos(alpha)

    g[2, 0]=g[0, 2]
    g[2, 1]=g[1, 2]
    g[2, 2]=c**2
    return g

    
def reciprocate(x, y, z, cell):
    "calculate miller indexes of a vector defined by its fractional cell coords"
    g = gtensor(cell)

    h=g[0, 0]*x+g[1, 0]*y+g[2, 0]*z;
    k=g[0, 1]*x+g[1, 1]*y+g[2, 1]*z;
    l=g[0, 2]*x+g[1, 2]*y+g[2, 2]*z;
    return h, k, l

def S2R(qx, qy, qz, cell):
    "Given cartesian coordinates of a vector in the S System, calculate its Miller indexes."
    #definire x y e z
    H=qx*x[0, :]+qy*y[0, :]+qz*z[0, :];
    K=qx*x[1, :]+qy*y[1, :]+qz*z[1, :];
    L=qx*x[2, :]+qy*y[2, :]+qz*z[2, :];
    q=np.sqrt(qx**2+qy**2+qz**2);
    return H, K, L, q

def R2S(x, y, z, H, K, L):
    "Given reciprocal-space coordinates of a vecotre, calculate its coordinates in the Cartesian space."
    qx=self.scalar(H, K, L, x[0, :], x[1, :], x[2, :], 'latticestar');
    qy=self.scalar(H, K, L, y[0, :], y[1, :], y[2, :], 'latticestar');
    qz=self.scalar(H, K, L, z[0, :], z[1, :], z[2, :], 'latticestar');
    q=self.modvec(H, K, L, 'latticestar');
    return qx, qy, qz, q

if __name__ == '__main__':
    # Parse options
    from optparse import OptionParser
    from phonopy.interface.vasp import read_vasp

    parser = OptionParser()
    parser.set_defaults( atom_num = 1,
                         pos_str = None )
    parser.add_option("-n", dest="atom_num", type="int",
			help="center atom")
    parser.add_option("-p", dest="pos_str", type="string",
			help="center position")
    (options, args) = parser.parse_args()


    cell = read_vasp('SPOSCAR')

    if options.pos_str == None:
        center = cell.get_scaled_positions()[ options.atom_num - 1 ]
    else:
        center = np.array( [ float(x) for x in options.pos_str.split() ] )

    for i in range(cell.get_number_of_atoms()):
        print ("%2d  %f" % (i+1, get_distance_with_center(cell, center, i)))
