# http://magcryst.org/resources/magnetic-coordinates/

import numpy as np

class MM(object):
    """
    Magnetic model class
    """
    __isfrozen = False
    def __setattr__(self, key, value):
        if self.__isfrozen and not hasattr(self, key):
            raise TypeError( "Cannot set attributes in this class" )
        object.__setattr__(self, key, value)

    def _freeze(self):
        self.__isfrozen = True
            
    def __init__(self, cell_size):
        try:
            self._size = int(cell_size)
        except:
            TypeError("Cannot parse size of mag model. Must be int.")
        self._fc = np.zeros([cell_size,3],dtype=np.complex)
        self._k = np.array([0,0,0])
        self._phi = np.zeros(cell_size,dtype=np.float)
        
        self._freeze() # no new attributes after this point. 

    @property
    def size(self):
        return self._size

    @property
    def k(self):
        return self._k

    @k.setter
    def k(self, value):
        if isinstance(value, np.ndarray):
            if value.shape == (3,):
                self._k=value
            else:
                raise RuntimeError
        else:
            raise RuntimeError
            
    @property
    def fc(self):
        return self._fc

    @fc.setter
    def fc(self, value):
        if isinstance(value, np.ndarray):
           
            if value.dtype != np.complex:
                raise RuntimeError
           
            if value.shape == self._fc.shape:                        
                self._fc=value
            else:
                raise RuntimeError("Invalid shape: {}".format(value.shape) + " instead of {}".format(self._fc.shape))
        else:
            raise RuntimeError


    @property
    def phi(self):
        return self._phi

    @phi.setter
    def phi(self, value):
        if isinstance(value, np.ndarray):
           
            if value.shape == self._phi.shape:
                self._phi=value
            else:
                raise RuntimeError
        else:
            raise RuntimeError
