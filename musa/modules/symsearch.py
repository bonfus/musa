import warnings

from musa.core.spg import spacegroup_from_data
from musa.core.parsers import *
#from musa.core.symmetry import Symmetry
from musa.core.nprint  import nprint, nprintmsg

have_spg = True

try:
    import spglib as spg
except ImportError:
    try:
        from pyspglib import spglib as spg
    except ImportError:
        nprint("Spg Library not loaded", "warn")
        have_spg = False


class SYMSEARCH():
    def __init__(self):
        if not have_spg:
            nprint("Symmetry find function not available." + \
                    "A warning will be raised", "warn")

        
    def sym_search(self):
        """
        Identifies symmetry operations of the unit cell
        """
        
        if self._check_lattice() and have_spg:
            symbol,number = spg.get_spacegroup(self._cell, symprec=1e-4).split()
            operations    = spg.get_symmetry(self._cell, symprec=1e-4)
            number = int(''.join([c for c in number if c.isdigit()]))
            self._sym = spacegroup_from_data(number,symbol,rotations=operations['rotations'],translations=operations['translations']) ## 
            return True
        elif not have_spg:
            warnings.warn("Warning, spglib not found. This function will always return False.")
            return False
        else:
            return False

    #def sym_print(self):
    #    
    #    if self._check_sym():        
    #        print ("Point group: "  + self._sym.get_pointgroup() )
    #        print ("Wyckoff: "  , self._sym.get_Wyckoff_letters() )
    #        print (self._sym.get_symop())
    #        print ('Pointgroup operations')
    #        print (self._sym.get_pointgroup_operations())
    #    
    #    return
    #    
    #    
    #def sym_get_pg(self):
    #    if self._check_sym(): 
    #        return self._sym.get_pointgroup_operations()
    #    else:
    #        return None
    #    
    #def sym_get_sym(self):
    #    if self._check_sym(): 
    #        return self._sym.get_symop()
    #    else:
    #        return None        
