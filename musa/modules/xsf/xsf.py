import os
import numpy as np
from copy import deepcopy

from musa.settings import config

from musa.modules.xsf.xsfio import *
from musa.modules.xsf.xsfrun import *

from musa.core.parsers import *
from musa.core.ninput import ninput
from musa.core.nprint import nprintmsg

class XSF(object):
    def __init__(self):
        pass

    
    def load_xsf(self, args):
        """Loads structural data from Xcrysden Files.
           :param file: file path.
        """
        try:
            fname = str(args)
        except:
            nprint ("Problems with file name...file not found!", 'warn')
            return
        atoms = read_xsf_file(fname)
        if atoms:
            self._cell = atoms
        else:
            nprint ("Atoms not loaded!", 'warn')
                
    
        
    def show_supercell(self, spSize=[1,1,1]):
        """Open XCrysDen with the currentlydefined supercell.
           WARNING: this can potentially harm your system!
        """
        try:
            ans = ninput('Are you sure?! [y/N]', parse_bool)
        except EOFError:
            return
        if ans:
            
            if not self._supercell is None:
                write_xsf(os.path.join(config.XCrysTmp,'preview.xsf'),self._supercell)
                run_xcrysden(os.path.join(config.XCrysTmp,'preview.xsf'))
            else:
                nprintmsg('esupcell')
                
            
    def show_cell(self):
        "Open XCrysDen with specified structure."        
        if not self._cell is None:
            c = deepcopy(self._cell)
            if self._muon:
                for m in self._muon:
                    c.extend(symbol="mu",scaled_position=m)
                
            write_xsf(os.path.join(config.XCrysTmp,'preview.xsf'),c)
            run_xcrysden(os.path.join(config.XCrysTmp,'preview.xsf'))
        else:
            nprintmsg('ecrystal')
        
