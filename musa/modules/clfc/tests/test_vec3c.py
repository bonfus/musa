import unittest
import ctypes, os
import numpy as np

dir = os.path.dirname(__file__)

testlib = ctypes.cdll.LoadLibrary(os.path.join(dir , '../clfclib.so'))

class vec3(ctypes.Structure):
     _fields_ = [("x", ctypes.c_double),
                ("y", ctypes.c_double),
                ("z", ctypes.c_double)]


class TestVec3Methods(unittest.TestCase):



	def test__vec3(self):
		testlib._vec3.restype = vec3
		## generate random vector
		rnd = np.random.rand(3)
		
		r = testlib._vec3(np.ctypeslib.as_ctypes(rnd[0]), \
							np.ctypeslib.as_ctypes(rnd[1]), \
							np.ctypeslib.as_ctypes(rnd[2]))
							
		self.assertEqual(r.x,rnd[0])
		self.assertEqual(r.y,rnd[1])
		self.assertEqual(r.z,rnd[2])
		
	def test_vec3_zero(self):
		testlib.vec3_zero.restype = vec3
		r = testlib.vec3_zero()
		self.assertEqual(r.x,0)
		self.assertEqual(r.y,0)
		self.assertEqual(r.z,0)
	
	def test_vec3_add(self):

		rnd1 = np.random.rand(3)
		rnd2 = np.random.rand(3)
		
		v1 = testlib._vec3(np.ctypeslib.as_ctypes(rnd1[0]), \
							np.ctypeslib.as_ctypes(rnd1[1]), \
							np.ctypeslib.as_ctypes(rnd1[2]))
		v2 = testlib._vec3(np.ctypeslib.as_ctypes(rnd2[0]), \
							np.ctypeslib.as_ctypes(rnd2[1]), \
							np.ctypeslib.as_ctypes(rnd2[2]))
							
		rndSum = rnd1 + rnd2
		
		testlib.vec3_add.restype = vec3
		r = testlib.vec3_add(v1,v2)

		self.assertEqual(r.x,rndSum[0])
		self.assertEqual(r.y,rndSum[1])
		self.assertEqual(r.z,rndSum[2])	
	
	def test_vec3_sub(self):

		rnd1 = np.random.rand(3)
		rnd2 = np.random.rand(3)
		
		v1 = testlib._vec3(np.ctypeslib.as_ctypes(rnd1[0]), \
							np.ctypeslib.as_ctypes(rnd1[1]), \
							np.ctypeslib.as_ctypes(rnd1[2]))
		v2 = testlib._vec3(np.ctypeslib.as_ctypes(rnd2[0]), \
							np.ctypeslib.as_ctypes(rnd2[1]), \
							np.ctypeslib.as_ctypes(rnd2[2]))
							
		rndDiff = rnd1 - rnd2
		
		testlib.vec3_sub.restype = vec3
		r = testlib.vec3_sub(v1,v2)

		self.assertEqual(r.x,rndDiff[0])
		self.assertEqual(r.y,rndDiff[1])
		self.assertEqual(r.z,rndDiff[2])	
		
	def test_vec3_mul(self):

		rnd1 = np.random.rand(3)
		rnd2 = np.random.rand(3)
		
		v1 = testlib._vec3(np.ctypeslib.as_ctypes(rnd1[0]), \
							np.ctypeslib.as_ctypes(rnd1[1]), \
							np.ctypeslib.as_ctypes(rnd1[2]))
		v2 = testlib._vec3(np.ctypeslib.as_ctypes(rnd2[0]), \
							np.ctypeslib.as_ctypes(rnd2[1]), \
							np.ctypeslib.as_ctypes(rnd2[2]))
							
		rndMult = np.multiply( rnd1, rnd2 )
		
		testlib.vec3_mul.restype = vec3
		r = testlib.vec3_mul(v1,v2)

		self.assertEqual(r.x,rndMult[0])
		self.assertEqual(r.y,rndMult[1])
		self.assertEqual(r.z,rndMult[2])	
		
	def test_vec3_muls(self):

		rnd1 = np.random.rand(3)
		rnd2 = np.random.rand(1)
		
		v1 = testlib._vec3(np.ctypeslib.as_ctypes(rnd1[0]), \
							np.ctypeslib.as_ctypes(rnd1[1]), \
							np.ctypeslib.as_ctypes(rnd1[2]))
		
		scalar = np.ctypeslib.as_ctypes(rnd2[0])
							
		rndMult = rnd2[0] * rnd1
		
		testlib.vec3_muls.restype = vec3
		r = testlib.vec3_muls(scalar,v1)

		self.assertEqual(r.x,rndMult[0])
		self.assertEqual(r.y,rndMult[1])
		self.assertEqual(r.z,rndMult[2])	
		
	def test_vec3_norm(self):

		rnd1 = np.random.rand(3)
		
		v1 = testlib._vec3(np.ctypeslib.as_ctypes(rnd1[0]), \
							np.ctypeslib.as_ctypes(rnd1[1]), \
							np.ctypeslib.as_ctypes(rnd1[2]))
									
		rndNorm = np.linalg.norm(rnd1)
		
		testlib.vec3_norm.restype = ctypes.c_double
		r = testlib.vec3_norm(v1)

		self.assertEqual(r,rndNorm)
		
	def test_vec3_dot(self):

		rnd1 = np.random.rand(3)
		rnd2 = np.random.rand(3)
		
		v1 = testlib._vec3(np.ctypeslib.as_ctypes(rnd1[0]), \
							np.ctypeslib.as_ctypes(rnd1[1]), \
							np.ctypeslib.as_ctypes(rnd1[2]))
		v2 = testlib._vec3(np.ctypeslib.as_ctypes(rnd2[0]), \
							np.ctypeslib.as_ctypes(rnd2[1]), \
							np.ctypeslib.as_ctypes(rnd2[2]))
									
		rndDot = np.dot(rnd1,rnd2)
		
		testlib.vec3_dot.restype = ctypes.c_double
		r = testlib.vec3_dot(v1,v2)

		self.assertEqual(r,rndDot)
		


if __name__ == '__main__':
    unittest.main()
