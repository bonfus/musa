from musa.core.nprint import nprint, nprintmsg
from musa.core.ninput import ninput
from musa.core.parsers import parse_int
from musa.core.cells   import get_supercell, get_surrounding_frame, get_diluted_supercell

from copy import deepcopy

import numpy as np


class Supercell(object):
    def __init__(self):
        pass

                

    def build_supercell(self, super_cell_matrix, only_magnetic = False):
        """
        Builds a supercell and adds it to the current environment
        super_cell_matrix : Diagonal matrix conteining the number of replica along a, b and c.
        only_magnetic     : Whether only magnetic atoms should be included
        returns: None
        """
        
        self._supercell = None
        
        if self._check_lattice():
            atoms = deepcopy(self._cell)
        else:
            nprintmsg('NS')
            return                
                
        if self._check_magdefs():
            fcs = self._fourier_components()
            if only_magnetic:
                n_rem = 0
                kept_fc = []
                
                for i, fc in enumerate(fcs):
                    if (fc == 0.+0j).all():
                        atoms.del_atom(i-n_rem)
                        n_rem += 1
                    else:
                        kept_fc.append(i)
                self._supercell = get_supercell(atoms,super_cell_matrix,FC=fcs[kept_fc,:],K=self._k(),PHI=self._phi())
            else:
                self._supercell = get_supercell(atoms,super_cell_matrix,FC=fcs,K=self._k(),PHI=self._phi())
        else:
            if only_magnetic:
                nprintmsg('NS')
                return                
            self._supercell = get_supercell(atoms,super_cell_matrix,FC=None,K=None,PHI=None)
            
        del atoms
        
        if self._check_muon():
            muons = self._get_muon_position(super_cell_matrix)
            for mp in muons:
                print (mp)
                self._supercell.extend(symbol="mu",scaled_position=mp)




    def _get_muon_position(self, scmat, ijk = None):
        '''
        Return absolute muon pos from crystal cell...try to put it in the middle.
        '''
        
        
        if ijk is None:
            i,j,k = np.floor(scmat/2).diagonal()
        else:
            i,j,k = ijk
        
        scaled_muon_pos = []
        for mp in self._muon:
            scaled_muon_pos.append([ (mp[0] + i) / scmat[0,0], (mp[1] + j) / scmat[1,1], (mp[2] + k) / scmat[2,2]])
            
                                                    
        return scaled_muon_pos


                
                
            


        
        
        

