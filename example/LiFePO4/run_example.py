# coding: utf-8
import numpy as np
from musa.musa import Musa

# Nice printing
np.set_printoptions(precision=4, suppress=True)


print('This is an interactive example!!\n')

m = Musa()
m.load_cif('./cifs/4001848.cif')

print('To set the magnetic order see:')
print('http://pubs.acs.org/doi/pdf/10.1021/cm0300462 or http://journals.aps.org/prb/pdf/10.1103/PhysRevB.73.024410')
print('\nTo check the results see:')
print('http://journals.aps.org/prb/pdf/10.1103/PhysRevB.84.054430\n')
print('Now set the magnetic order:')

m.mago_add()
m.muon_set_frac([0.1225,0.3772,0.8])
m.muon_set_frac([0.0416,0.2500,0.9])
m.muon_set_frac([0.3901,0.2500,0.3])
m.muon_set_frac([0.8146,0.0404,0.8])
print('Local fields at the muon sites (Tesla):')
r = m.locfield([100,100,100],200)


print(r[0].D, "Norm {: 0.4f}".format(np.linalg.norm(r[0].T)))
print(r[1].D, "Norm {: 0.4f}".format(np.linalg.norm(r[1].T)))
print(r[2].D, "Norm {: 0.4f}".format(np.linalg.norm(r[2].T)))
print(r[3].D, "Norm {: 0.4f}".format(np.linalg.norm(r[3].T)))


# ==== Now Automatically ====

print('Now automatic run...\n')

from musa.core.magmodel import MM

# prepare a Magnetic Model 
mm = MM(28)  # 28 is the number of atoms in the unit cell. Each atom must a a defined value
# propagation vector, defined with respoect to the cel lattice vectors
mm.k=np.array([0.,0.,0.])
# Fourier components, in CARTESIAN coordinates!
mm.fc=np.array([[ 0.00+0.j,  4.19+0.j,  0.00+0.j],
       [ 0.00+0.j, -4.19+0.j,  0.00+0.j],
       [ 0.00+0.j, -4.19+0.j,  0.00+0.j],
       [ 0.00+0.j,  4.19+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j],
       [ 0.00+0.j,  0.00+0.j,  0.00+0.j]])

# We add the magnetic model. The last order added is automatically selected
m.mago_add(mm)


print('Local fields at the muon sites (Tesla):')
r = m.locfield([100,100,100],200)

print(r[0].D/4.19, "Norm {: 0.4f}".format(np.linalg.norm(r[0].T/4.19)))
print(r[1].D/4.19, "Norm {: 0.4f}".format(np.linalg.norm(r[1].T/4.19)))
print(r[2].D/4.19, "Norm {: 0.4f}".format(np.linalg.norm(r[2].T/4.19)))
print(r[3].D/4.19, "Norm {: 0.4f}".format(np.linalg.norm(r[3].T/4.19)))
