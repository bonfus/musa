from musa.core.nprint import nprint, nprintmsg, print_cell



class Print(object):
    def __init__(self):
        pass
    
  
        
    def print_cell(self):
        """
        Print base cell
        """
        if self._check_lattice():
            print_cell(self._cell)
                
            
    def print_supercell(self):
        """
        Print super cell
        """
        if self._check_supercell():
            print_cell(self._supercell)
            
        
   
