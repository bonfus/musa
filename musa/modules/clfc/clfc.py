import os
import numpy as np
import ctypes
from numpy.ctypeslib import ndpointer
from copy import deepcopy

from musa.core.parsers import *
from musa.core.nprint  import nprint, nprintmsg
from musa.core.ninput  import ninput, ninput_mt


try:
    import lfcext 
except:
    print("LFC Extension not found!")


class LocalFields(object):
    """
    Object containing local field components.
    All units are assumed as S.I.
    

    Properties

    ACont : float
        Contact hyperfine coupling. Units are :math:`Angstrom^{-3}`
    
    L : array
        Lorentz field. Units are Tesla

    Lorentz : array
        Lorentz field. Units are Tesla

    D : array
        Dipolar field. Units are Tesla

    Dipolar : array
        Dipolar field. Units are Tesla

    C : array
        Dipolar field. Units are Tesla

    Contact : array
        Contact hyperfine field. 
        It is obtained from :math:`ACont \\cdot \\frac{2 \\mu_0}{3} \\sum_{r_{cont}} m`
        Units are Tesla
        

    """

    __isfrozen = False
    def __setattr__(self, key, value):
        if self.__isfrozen and not hasattr(self, key):
            raise TypeError( "Cannot set attributes in this class" )
        object.__setattr__(self, key, value)

    def _freeze(self):
        self.__isfrozen = True

    def __init__(self, BCont, BDip, BLor, ACont=0.):

        assert(type(BLor) is np.ndarray)
        assert(type(BDip) is np.ndarray)
        assert(type(BCont) is np.ndarray)
        
        assert (BDip.shape == BCont.shape == BLor.shape)
        
        
        self._BLor = BLor
        self._BDip = BDip
        self._BCont = BCont
        try:
            self._ACont = np.float(ACont)
        except:
            raise TypeError( "Cannot set value for ACont" )
        
        self._freeze() # no new attributes after this point. 
        
    def __repr__(self):
        return (self._BLor + self._BDip + self._ACont*self._BCont).__repr__()
    
    @property
    def L(self):
        return self._BLor
        
    @property
    def Lorentz(self):
        return self._BLor
        
    @property
    def D(self):
        return self._BDip
        
    @property
    def Dipolar(self):
        return self._BDip

    @property
    def C(self):
        return self._ACont*self._BCont
        
    @property
    def Contact(self):
        return self._ACont*self._BCont
        
    @property
    def T(self):
        return self._BLor + self._BDip + self._ACont*self._BCont
        
    @property
    def Total(self):
        return self._BLor + self._BDip + self._ACont*self._BCont
        
    @property
    def ACont(self):
        return self._ACont
    @ACont.setter
    def ACont(self,value):
        try:
            self._ACont = np.float(value)
        except:
            raise TypeError( "Cannot set value for ACont" )




class LFC(object):
    """
    Class providing a simple interface to the LFC Extension.
        

    """    
    def __init__(self):
        pass
    def locfield(self, ctype, supercellsize, radius, nnn = 2, rcont = 10.0, nangles = None, axis = None):
        """
        Local fields at the muon site.
        
        Returns a LocalFields object.
        """
        
        
        # check current status is ok
        if not (self._check_lattice() and self._check_magdefs()):
            return []
        
        if type(ctype) != str:
            raise TypeError("ctype must be a of type str")
            
        # validate input
        if ctype != 's' and ctype != 'sum' and \
           ctype != 'r' and ctype != 'rotate' and  \
           ctype != 'i' and ctype != 'incommmensurate':
            raise ValueError("Invalid calculation type.")
        
        # if 'i', nangles must be defined
        if ctype == 'i' or ctype == 'incommmensurate' or \
           ctype == 'r' or ctype == 'rotate':
            if nangles is None:
                raise ValueError("Number of angles must be specified.")
            try:
                nangles  = int(nangles)
            except:
                raise ValueError("Cannot convert number of angles to int.")
        if ctype == 'r' or ctype == 'rotate':
            if axis is None:
                raise ValueError("Axis for rotation must be specified.")
            try:
                axis = np.array(axis)
                axis = axis/np.linalg.norm(axis)
            except:
                raise ValueError("Cannot convert axis for rotation to np.ndarray.")
        
        try:
            sc = np.array(supercellsize, dtype=np.int32)
        except:
            raise TypeError("Cannot convert supercellsize to NumPy array.")
            
        if sc.shape != (3,):
            raise ValueError("Propagation vector has the wrong shape.")
        
        try:
            r= float(radius) # Lorentz radius (in A)
        except:
            raise TypeError("Cannot convert radius to float.")
        
        try:
            nnn = int(nnn)
        except:
            raise TypeError("Cannot convert nnn to int.")
            
        if nnn<0:
            raise ValueError("nnn must be positive.")
        
        rc=0
        try:
            rc = float(rcont)
        except:
            raise TypeError("Cannot convert rcont to float.")
            
        if rc<0:
            raise ValueError("rcont must be positive.")
        
        
        
        # Remove non magnetic atoms from list

        unitcell = deepcopy(self._cell) #why?
        positions = unitcell.get_scaled_positions()
        ufc = self._fourier_components()    
        latpar = unitcell.get_cell()
        magnetic_atoms=[]
        for i, e in enumerate(ufc):
            if not np.allclose(e,np.zeros(3,dtype=np.complex)):
                magnetic_atoms.append(i)

        p = positions[magnetic_atoms,:]
        fc = ufc[magnetic_atoms,:]
        phi = self._phi()[magnetic_atoms] # phase in magnetic order definition
        k = self._k()        
        

        
        res = []
        # if is outside for (minimal) sake of performances
        if ctype == 's' or ctype == 'sum':
            for m in range(len(self._muon)):
                mu = self._muon[m]  # muon pos
                res.append(LocalFields(*lfcext.Fields(ctype, p,fc,k,phi,mu,sc,latpar,r,nnn,rc)))
        elif ctype == 'i' or ctype == 'incommensurate':
            for m in range(len(self._muon)):
                mu = self._muon[m]  # muon pos
                res.append(LocalFields(*lfcext.Fields(ctype, p,fc,k,phi,mu,sc,latpar,r,nnn,rc,nangles)))
        elif ctype == 'r' or ctype == 'rotate':
            for m in range(len(self._muon)):
                mu = self._muon[m]  # muon pos
                res.append(LocalFields(*lfcext.Fields(ctype, p,fc,k,phi,mu,sc,latpar,r,nnn,rc,nangles,axis)))
            
        
        return res
        
    
    def dipten(self, supercellsize, radius):
        """
        Calculates dipolar tensor for given muon sites.
        
        A fake magnetic order must be defined. This is only used to 
        distinguish the ``magnetically active'' atoms for which the 
        calculation is perfomred.
        
        The results are provided in 1/Angstrom^3.
        The conversion factor to mol/emu is 1.6605389 which is given by
        
        .. math::
        
           (1 angstrom^{-3} )/(1cm^{-3}) = 1e24
           
           1/N_A = 1.6605E-24 mole
                    
        """
        
        # check current status is ok
        if not (self._check_lattice() and self._check_magdefs()):
            return []
        
        try:
            r= float(radius) # Lorentz radius (in A)
            if r<0:
                raise ValueError("Lorentz radius must be greater or equal to 0.")
        except:
            raise TypeError("Cannot convert radius to float.")
        
        
        try:
            sc = np.array(supercellsize, dtype=np.int32)
            if sc.shape != (3,):
                raise ValueError("Supercellsize has wrong shape.")
        except:
            raise TypeError("Cannot convert supercellsize to NumPy array.")
                    
        # Remove non magnetic atoms from list

        unitcell = deepcopy(self._cell) #why?
        positions = unitcell.get_scaled_positions()
        ufc = self._fourier_components()    
        latpar = unitcell.get_cell()
        magnetic_atoms=[]
        for i, e in enumerate(ufc):
            if not np.allclose(e,np.zeros(3,dtype=np.complex)):
                magnetic_atoms.append(i)

        p = positions[magnetic_atoms,:]
      
        res = []
        for m in range(len(self._muon)):
            mu = self._muon[m]  # muon pos
            res.append(lfcext.DipolarTensor(p,mu,sc,latpar,r))

        return res        

#class CLFC(object):  # Local Field contributions in C
#    def __init__(self):
#        dir = os.path.dirname(__file__)
#
#        self._lib = ctypes.cdll.LoadLibrary(dir+"/clfclib.so")
#        
#
#        # set ASS calling parameters
#        self._ass = self._lib.ASS
#        self._ass.restype = None
#    
#        #    Declaration
#        #        void ASS(const double *in_positions, 
#        #                const double *in_fc, const double *in_K, const double *in_phi,
#        #                const double *in_muonpos, const int * in_supercell, const double *in_cell, 
#        #                const double radius, const size_t nnn_for_cont, const double cont_radius, 
#        #                size_t size,
#        #                double *out_field_cont, double *out_field_dip, double *out_field_lor)         
#        self._ass.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"), # in_positions   ->
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),       # in_fc          ->
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),       # in_K           ->
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),       # in_phi         ->
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),       # in_muonpos     ->
#                        ctypes.POINTER(ctypes.c_int),                           # in_supercell   ->
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),       # in_cell        ->
#                        ctypes.c_double,                                        # radius         ->
#                        ctypes.c_uint,                                          # nnn_for_cont   ->
#                        ctypes.c_double,                                        # cont_radius    ->
#                        ctypes.c_uint,                                          # size           -> number of magnetic atoms
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),       # out_field_cont -> 
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),       # out_field_dip  -> 
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")]       # out_field_lor  -> 
#
#        # set DT calling parameters
#        self._dt = self._lib.DT
#        self._dt.restype = None
#    
#        #    Declaration
#        #       void DT(const double *in_positions, 
#        #                 const double *in_muonpos, const int * in_supercell, const double *in_cell, 
#        #                 const double radius, size_t size,
#        #                 double *out_field)         
#    
#        self._dt.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#                        ctypes.POINTER(ctypes.c_int),
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#                        ctypes.c_double,
#                        ctypes.c_size_t,
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")]     
#
#        # set RASS calling parameters
#        self._rass = self._lib.RASS
#        self._rass.restype = None
#        
#        #    Declaration
#        #        void RASS(const double *in_positions, 
#        #        		  const double *in_fc, const double *in_K, const double *in_phi,
#        #                  const double *in_muonpos, const int * in_supercell, const double *in_cell, 
#        #                  const double radius, const size_t nnn_for_cont, const double cont_radius, 
#        #                  size_t size, 
#        #                  const double *in_axis, size_t in_nangles,
#        #                  double *out_field_cont, double *out_field_dip, double *out_field_lor)
#        
#        self._rass.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"), #  in_positions
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),        #  in_fc
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),        #  in_K
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),        #  in_phi
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),        #  in_muonpos
#                        ctypes.POINTER(ctypes.c_int),                            #  in_supercell
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),        #  in_cell
#                        ctypes.c_double,                                         #  radius
#                        ctypes.c_size_t,                                         #  nnn_for_cont
#                        ctypes.c_double,                                         #  cont_radius
#                        ctypes.c_size_t,                                         #  size           -> number of magnetic atoms
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),        #  in_axis
#                        ctypes.c_size_t,                                         #  in_nangles
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),        #  out_field_cont
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),        #  out_field_dip
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")]        #  out_field_lor
#                        
#
#        # set INCASS calling parameters
#        self._incass = self._lib.INCASS
#        self._incass.restype = None
#        
#        #    Declaration
#        #
#        #        void INCASS(const double *in_positions, 
#        #                  const double *in_fc, const double *in_K, const double *in_phi,
#        #                  const double *in_muonpos, const int * in_supercell, const double *in_cell, 
#        #                  const double radius, const size_t nnn_for_cont, const double cont_radius, 
#        #                  size_t size, size_t in_nangles,
#        #                  double *out_field_cont, double *out_field_dip, double *out_field_lor)         
#        
#        self._incass.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),  #  in_positions
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),           #  in_fc
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),           #  in_K
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),           #  in_phi
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),           #  in_muonpos
#                        ctypes.POINTER(ctypes.c_int),                               #  in_supercell
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),           #  in_cell
#                        ctypes.c_double,                                            #  radius
#                        ctypes.c_size_t,                                            #  nnn_for_cont
#                        ctypes.c_double,                                            #  cont_radius                        
#                        ctypes.c_size_t,                                            #  size
#                        ctypes.c_size_t,                                            #  in_angles
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),           #  out_field_cont
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),           #  out_field_dip
#                        ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")]           #  out_field_lor
#                        
#        ##                
#        ## set RASSHF calling parameters
#        ## 
#        #self._rasshf = self._lib.RASSHF
#        #self._rasshf.restype = None
#        ##    Declaration
#        ##        
#        ##       void RASSHF(const double *in_positions, const double *in_fc, const double *in_K,
#        ##         const double *in_muonpos, const int * in_supercell, const double *in_cell, 
#        ##         const double radius, size_t size, const double *in_axis, size_t in_nangles,
#        ##         const double Acont, const double Rcont, double *out_bdip, double *out_bcont) 
#        ##
#        #self._rasshf.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#        #                ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#        #                ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#        #                ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#        #                ctypes.POINTER(ctypes.c_int),
#        #                ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#        #                ctypes.c_double,
#        #                ctypes.c_size_t,
#        #                ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
#        #                ctypes.c_size_t,
#        #                ctypes.c_double,
#        #                ctypes.c_double,
#        #                ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),                        
#        #                ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")]
#                                
#                            
#        
#
############################## DIPOLAR SUMS #############################            
#  
#    def _runass(self,p,fc,k,phi,mu,sc,c,r,nnn,rcont,size):
#        odip = np.zeros(3) 
#        olor = np.zeros(3) 
#        ocont = np.zeros(3) 
#        
#        self._ass(np.ascontiguousarray(p, np.float64),\
#                  np.ascontiguousarray(fc, np.float64),\
#                  np.ascontiguousarray(k, np.float64),\
#                  np.ascontiguousarray(phi, np.float64),\
#                  np.ascontiguousarray(mu, np.float64),\
#                  sc,\
#                  np.ascontiguousarray(c, np.float64),\
#                  r,\
#                  nnn,\
#                  rcont,\
#                  size,\
#                  np.ascontiguousarray(ocont, np.float64),\
#                  np.ascontiguousarray(odip, np.float64),\
#                  np.ascontiguousarray(olor, np.float64))
#        return LocalFields(ocont, odip, olor)
#    
#    def locfield(self, supercellsize, radius, nnn = 2, rcont = 10.0):
#        """
#        Calculates the local field as the sum of the Dipolar field and the Lorentz field.
#        """
#        
#        if not self._check_lattice():
#            nprintmsg('NS')
#            return []
#                    
#        # copy the unit cell from current loaded cell
#        unitcell = deepcopy(self._cell)
#        
#        positions = unitcell.get_scaled_positions()
#
#        ufc = self._fourier_components()
#    
#        latpar = unitcell.get_cell()
#        # this will contain the positions of magnetic atoms
#        p = []
#        # this will contain the fourier components of magnetic atoms
#        fc = []
#        
#        for i, e in enumerate(ufc):
#            if not np.allclose(e.real, np.array([0.,0.,0.])): 
#                fc += e.real.tolist()
#                fc += e.imag.tolist()
#                p += positions[i].tolist()
#        # makes array flat to pass it to the C library
#        p = np.array(p).flatten()
#        fc = np.array(fc)  # fc
#        k = np.array(self._k())  # k
#        IntArray3 = ctypes.c_int * 3
#        sc = IntArray3(*supercellsize)  # dimension of the supercell
#        c = latpar.flatten()  # lattice parameters
#        r= radius # Lorentz radius (in A)
#        phi = self._phi() # phase in magnetic order definition
#        size = int(len(p)/3) # number of atoms. Only mag atoms are included
#        
#        
#        res = []
#        for m in range(len(self._muon)):
#        
#            mu = self._muon[m]  # muon pos
#            res.append ( self._runass(p,fc,k,phi,mu,sc,c,r,nnn,rcont,size) )
#        
#        return res
#
#
###########################################################################
#
#
######################### DIPOLAR SUMS With ROTATIONS #####################
#  
#    def _runrass(self,p,fc,k,phi,mu,sc,c,r,nnn,rcont,na,axis,size):
#                
#        odip = np.zeros(3*na)          # hosting output
#        olor = np.zeros(3*na) 
#        ocont = np.zeros(3*na) 
#        
#        a = axis / np.linalg.norm(axis)
#        
#        #self._rass(p,fc,k,mu,sc,c,r,size,a,na,o)
#        
#        
#        self._rass(np.ascontiguousarray(p, np.float64),\
#                  np.ascontiguousarray(fc, np.float64),\
#                  np.ascontiguousarray(k, np.float64),\
#                  np.ascontiguousarray(phi, np.float64),\
#                  np.ascontiguousarray(mu, np.float64),\
#                  sc,\
#                  np.ascontiguousarray(c, np.float64),\
#                  r,\
#                  nnn,\
#                  rcont,\
#                  size,\
#                  np.ascontiguousarray(a, np.float64),\
#                  na,\
#                  np.ascontiguousarray(ocont, np.float64),\
#                  np.ascontiguousarray(odip, np.float64),\
#                  np.ascontiguousarray(olor, np.float64))
#                
#        
#        
#        odip = odip.reshape([na,3])
#        olor = olor.reshape([na,3])
#        ocont = ocont.reshape([na,3])
#        return LocalFields(ocont, odip, olor)
#    
#    def locfieldwrotation(self, supercellsize, radius, axis, nangles, nnn = 2, rcont = 10.0):
#        """
#        Calculates the local field as the sum of the Dipolar field and the Lorentz field.
#        The local moments are rotated along axis.
#        nangles are sampled
#        """
#
#        if not self._check_lattice():
#            nprintmsg('NS')
#            return []        
#        
#        # copy the unit cell from current loaded cell
#        unitcell = deepcopy(self._cell)
#        
#        positions = unitcell.get_scaled_positions()
#
#        ufc = self._fourier_components()
#    
#        latpar = unitcell.get_cell()
#        # this will contain the positions of magnetic atoms
#        p = []
#        # this will contain the fourier components of magnetic atoms
#        fc = []
#        
#        for i, e in enumerate(ufc):
#            if not np.allclose(e.real, np.array([0.,0.,0.])): 
#                fc += e.real.tolist()
#                fc += e.imag.tolist()
#                p += positions[i].tolist()
#        # makes array flat to pass it to the C library
#        p = np.array(p).flatten()
#        fc = np.array(fc)  # fc
#        k = np.array(self._k())  # k
#        IntArray3 = ctypes.c_int * 3
#        sc = IntArray3(*supercellsize)  # dimension of the supercell
#        c = latpar.flatten()  # lattice parameters
#        r= radius # Lorentz radius (in A)
#        phi = self._phi() # phase in magnetic order definition
#        a = axis / np.linalg.norm(axis)        
#        size = int(len(p)/3) # number of atoms. Only mag atoms are included
#        
#        
#        res = [] #np.zeros([len(self._muon),3])
#        for m in range(len(self._muon)):
#        
#            mu = self._muon[m]  # muon pos
#            res.append(self._runrass(p,fc,k,phi,mu,sc,c,r,nnn,rcont,nangles,a,size))
#        
#        return res
#
#
###########################################################################
#
############# DIPOLAR SUMS With ROTATIONS and Contact Term #################
##  
##    def _runrasshf(self,p,fc,k,phi,mu,sc,c,r,na,axis, acont, rcont, size):
##        od = np.zeros(3*na)         # hosting output for dipolar sum
##        oc = np.zeros(3*na)         # hosting output for contact
##        a = axis / np.linalg.norm(axis)
##        self._rasshf(p,fc,k,mu,sc,c,r,size,a,na,acont,rcont,od,oc)
##        
##        od = od.reshape([na,3])
##        oc = oc.reshape([na,3])
##        return  [od,oc]  
##    
##    def locfieldwrotationhf(self, supercellsize, radius, axis, nangles, Acont, Rcont):
##        """
##        Calculates the local field as the sum of the Dipolar field and the Lorentz field.
##        The local moments are rotated along axis.
##        nangles are sampled
##        """
##        # copy the unit cell from current loaded cell
##        unitcell = deepcopy(self._cell)
##        
##        positions = unitcell.get_scaled_positions()
##
##        ufc = self._fourier_components()
##    
##        latpar = unitcell.get_cell()
##        # this will contain the positions of magnetic atoms
##        p = []
##        # this will contain the fourier components of magnetic atoms
##        fc = []
##        
##        for i, e in enumerate(ufc):
##            if not np.allclose(e.real, np.array([0.,0.,0.])): 
##                fc += e.real.tolist()
##                fc += e.imag.tolist()
##                p += positions[i].tolist()
##        # makes array flat to pass it to the C library
##        p = np.array(p).flatten()
##        fc = np.array(fc)  # fc
##        k = np.array(self._k())  # k
##        IntArray3 = ctypes.c_int * 3
##        sc = IntArray3(*supercellsize)  # dimension of the supercell
##        c = latpar.flatten()  # lattice parameters
##        r= radius # Lorentz radius (in A)
##        phi = self._phi() # phase in magnetic order definition
##        a = axis / np.linalg.norm(axis)        
##        size = int(len(p)/3) # number of atoms. Only mag atoms are included
##        
##        
##        res = [] #np.zeros([len(self._muon),3])
##        for m in range(len(self._muon)):
##        
##            mu = self._muon[m]  # muon pos
##            res.append(self._runrasshf(p,fc,k,phi,mu,sc,c,r,nangles,a,Acont,Rcont,size))
##        
##        return res
##
##
############################################################################
#
#
################# DIPOLAR SUMS With INCOMMENSURATE STRUCTURES #############
#  
#    #def _runincass(self,p,fc,k,phi,mu,sc,c,r,na, nangles):
#    def _runincass  (self,p,fc,k,phi,mu,sc,c,r,nnn,rcont,nang,size):
#                
#        odip = np.zeros(3*nang)          # hosting output
#        olor = np.zeros(3*nang) 
#        ocont = np.zeros(3*nang) 
#        
#        self._incass(np.ascontiguousarray(p, np.float64),\
#                  np.ascontiguousarray(fc, np.float64),\
#                  np.ascontiguousarray(k, np.float64),\
#                  np.ascontiguousarray(phi, np.float64),\
#                  np.ascontiguousarray(mu, np.float64),\
#                  sc,\
#                  np.ascontiguousarray(c, np.float64),\
#                  r,\
#                  nnn,\
#                  rcont,\
#                  size,\
#                  nang,\
#                  np.ascontiguousarray(ocont, np.float64),\
#                  np.ascontiguousarray(odip, np.float64),\
#                  np.ascontiguousarray(olor, np.float64))
#        
#        
#        odip = odip.reshape([nang,3])
#        olor = olor.reshape([nang,3])
#        ocont = ocont.reshape([nang,3])
#        return LocalFields(ocont, odip, olor)
#    
#    def locfieldincom(self, supercellsize, radius, nangles, nnn = 2, rcont = 10.0):
#        """
#        Calculates the local field as the sum of the Dipolar field and the Lorentz field.
#        The local moments are rotated along axis.
#        nangles are sampled
#        """
#        
#        if not self._check_lattice():
#            nprintmsg('NS')
#            return []
#        
#        # copy the unit cell from current loaded cell
#        unitcell = deepcopy(self._cell)
#        
#        positions = unitcell.get_scaled_positions()
#
#        ufc = self._fourier_components()
#    
#        latpar = unitcell.get_cell()
#        # this will contain the positions of magnetic atoms
#        p = []
#        # this will contain the fourier components of magnetic atoms
#        fc = []
#        
#        for i, e in enumerate(ufc):
#            if not np.allclose(e.real, np.array([0.,0.,0.])): 
#                fc += e.real.tolist()
#                fc += e.imag.tolist()
#                p += positions[i].tolist()
#        # makes array flat to pass it to the C library
#        p = np.array(p).flatten()
#        fc = np.array(fc)  # fc
#        k = np.array(self._k())  # k
#        IntArray3 = ctypes.c_int * 3
#        sc = IntArray3(*supercellsize)  # dimension of the supercell
#        c = latpar.flatten()  # lattice parameters
#        r= radius # Lorentz radius (in A)
#        phi = self._phi() # phase in magnetic order definition
#        size = int(len(p)/3) # number of atoms. Only mag atoms are included
#        
#        
#        res = [] #np.zeros([len(self._muon),3])
#        for m in range(len(self._muon)):
#        
#            mu = self._muon[m]  # muon pos
#            res.append(self._runincass(p,fc,k,phi,mu,sc,c,r,nnn,rcont,nangles,size))
#
#        return res
#
#
###########################################################################
#
#
#
############################## DIPOLAR TENSOR #############################            
#
#    def _rundt(self,p,mu,sc,c,r,size):
#        o = np.zeros(9)
#        self._dt(np.ascontiguousarray(p, np.float64),\
#                  np.ascontiguousarray(mu, np.float64),\
#                  sc,\
#                  np.ascontiguousarray(c, np.float64),\
#                  r,\
#                  size,\
#                  np.ascontiguousarray(o, np.float64))
#        return o
#    
#    def dipten(self, supercellsize, radius):
#        """
#        Calculates the dipolar tensor
#        """
#        # copy the unit cell from current loaded cell
#        unitcell = deepcopy(self._cell)
#        
#        positions = unitcell.get_scaled_positions()
#
#        ufc = self._fourier_components()
#    
#        latpar = unitcell.get_cell()
#        # this will contain the positions of magnetic atoms
#        p = []
#        # this will contain the fourier components of magnetic atoms
#        fc = []
#        
#        for i, e in enumerate(ufc):
#            if not np.allclose(e.real, np.array([0.,0.,0.])): 
#                fc += e.real.tolist()
#                fc += e.imag.tolist()
#                p += positions[i].tolist()
#        # makes array flat to pass it to the C library
#        p = np.array(p).flatten()
#        IntArray3 = ctypes.c_int * 3
#        sc = IntArray3(*supercellsize)  # dimension of the supercell
#        c = latpar.flatten()  # lattice parameters
#        r= radius # Lorentz radius (in A)
#        size = int(len(p)/3) # number of atoms. Only mag atoms are included
#        
#        
#        res = np.zeros([len(self._muon),9])
#        for m in range(len(self._muon)):
#        
#            mu = self._muon[m]  # muon pos
#            res[m] = self._rundt(p,mu,sc,c,r,size)
#        
#        return res
#
###########################################################################
