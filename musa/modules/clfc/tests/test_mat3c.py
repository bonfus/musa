import unittest
import ctypes
import math, os
import numpy as np


dir = os.path.dirname(__file__)

testlib = ctypes.cdll.LoadLibrary(dir + '/../clfclib.so')

class vec3(ctypes.Structure):
     _fields_ = [("x", ctypes.c_double),
                ("y", ctypes.c_double),
                ("z", ctypes.c_double)]
class mat3(ctypes.Structure):
     _fields_ = [("a", vec3),
                ("b", vec3),
                ("c", vec3)]


class TestMat3Methods(unittest.TestCase):



	def test_mat3_zero(self):
		testlib.mat3_zero.restype = mat3
		r = testlib.mat3_zero()
		self.assertEqual(r.a.x,0)
		self.assertEqual(r.a.y,0)
		self.assertEqual(r.a.z,0)
		self.assertEqual(r.b.x,0)
		self.assertEqual(r.b.y,0)
		self.assertEqual(r.b.z,0)
		self.assertEqual(r.c.x,0)
		self.assertEqual(r.c.y,0)
		self.assertEqual(r.c.z,0)

	def test_mat3_identity(self):
		testlib.mat3_identity.restype = mat3
		r = testlib.mat3_identity()
		self.assertEqual(r.a.x,1.)
		self.assertEqual(r.a.y,0)
		self.assertEqual(r.a.z,0)
		self.assertEqual(r.b.x,0)
		self.assertEqual(r.b.y,1.)
		self.assertEqual(r.b.z,0)
		self.assertEqual(r.c.x,0)
		self.assertEqual(r.c.y,0)
		self.assertEqual(r.c.z,1.)		
	

	def test_mat3_diag(self):
		
		
		rnd1 = np.random.rand(3)
		
		testlib.mat3_diag.restype = mat3
		r = testlib.mat3_diag(np.ctypeslib.as_ctypes(rnd1[0]), \
							np.ctypeslib.as_ctypes(rnd1[1]), \
							np.ctypeslib.as_ctypes(rnd1[2]))		
		
		self.assertEqual(r.a.x,rnd1[0])
		self.assertEqual(r.a.y,0)
		self.assertEqual(r.a.z,0)
		self.assertEqual(r.b.x,0)
		self.assertEqual(r.b.y,rnd1[1])
		self.assertEqual(r.b.z,0)
		self.assertEqual(r.c.x,0)
		self.assertEqual(r.c.y,0)
		self.assertEqual(r.c.z,rnd1[2])		
	

	def test_mat3_mul(self):
		
		
		rnd1 = np.matrix(np.random.rand(3,3))
		rnd2 = np.matrix(np.random.rand(3,3))
		
		m1 = mat3()
		m2 = mat3()
		
		m1.a.x = rnd1[0,0]; m1.a.y = rnd1[0,1]; m1.a.z = rnd1[0,2]
		m1.b.x = rnd1[1,0]; m1.b.y = rnd1[1,1]; m1.b.z = rnd1[1,2]
		m1.c.x = rnd1[2,0]; m1.c.y = rnd1[2,1]; m1.c.z = rnd1[2,2]
		
		m2.a.x = rnd2[0,0]; m2.a.y = rnd2[0,1]; m2.a.z = rnd2[0,2]
		m2.b.x = rnd2[1,0]; m2.b.y = rnd2[1,1]; m2.b.z = rnd2[1,2]
		m2.c.x = rnd2[2,0]; m2.c.y = rnd2[2,1]; m2.c.z = rnd2[2,2]
		
		testlib.mat3_mul.restype = mat3
		r = testlib.mat3_mul(m1,m2)		
		
		rndMul = rnd1*rnd2
		
		self.assertEqual(r.a.x,rndMul[0,0])
		self.assertEqual(r.a.y,rndMul[0,1])
		self.assertEqual(r.a.z,rndMul[0,2])
		self.assertEqual(r.b.x,rndMul[1,0])
		self.assertEqual(r.b.y,rndMul[1,1])
		self.assertEqual(r.b.z,rndMul[1,2])
		self.assertEqual(r.c.x,rndMul[2,0])
		self.assertEqual(r.c.y,rndMul[2,1])
		self.assertEqual(r.c.z,rndMul[2,2])

	def test_mat3_mulv(self):
		
		
		rnd1 = np.matrix(np.random.rand(3,3))
		rnd2 = np.matrix(np.random.rand(3))
		
		m1 = mat3()
		v2 = vec3()
		
		m1.a.x = rnd1[0,0]; m1.a.y = rnd1[0,1]; m1.a.z = rnd1[0,2]
		m1.b.x = rnd1[1,0]; m1.b.y = rnd1[1,1]; m1.b.z = rnd1[1,2]
		m1.c.x = rnd1[2,0]; m1.c.y = rnd1[2,1]; m1.c.z = rnd1[2,2]
		
		v2.x = rnd2[0,0]; v2.y = rnd2[0,1]; v2.z = rnd2[0,2]
		
		testlib.mat3_mulv.restype = vec3
		r = testlib.mat3_mulv(m1,v2)		
		
		rndMul = rnd1*rnd2.T
		
		self.assertEqual(r.x,rndMul[0,0])
		self.assertEqual(r.y,rndMul[1,0])
		self.assertEqual(r.z,rndMul[2,0])	

	def test_mat3_vmul(self):
		
		
		rnd1 = np.matrix(np.random.rand(3,3))
		rnd2 = np.matrix(np.random.rand(3))
		
		m1 = mat3()
		v2 = vec3()
		
		m1.a.x = rnd1[0,0]; m1.a.y = rnd1[0,1]; m1.a.z = rnd1[0,2]
		m1.b.x = rnd1[1,0]; m1.b.y = rnd1[1,1]; m1.b.z = rnd1[1,2]
		m1.c.x = rnd1[2,0]; m1.c.y = rnd1[2,1]; m1.c.z = rnd1[2,2]
		
		v2.x = rnd2[0,0]; v2.y = rnd2[0,1]; v2.z = rnd2[0,2]
		
		testlib.mat3_vmul.restype = vec3
		r = testlib.mat3_vmul(v2,m1)
		
		rndMul = rnd2*rnd1
		
		self.assertEqual(r.x,rndMul[0,0])
		self.assertEqual(r.y,rndMul[0,1])
		self.assertEqual(r.z,rndMul[0,2])
	
	
	def test_mat3_aangle(self):
		# rotation matrix taken from http://www.lfd.uci.edu/~gohlke/code/transformations.py.html
		def unit_vector(data, axis=None, out=None):
			"""Return ndarray normalized by length, i.e. Euclidean norm, along axis.
		
			>>> v0 = np.random.random(3)
			>>> v1 = unit_vector(v0)
			>>> np.allclose(v1, v0 / np.linalg.norm(v0))
			True
			>>> v0 = np.random.rand(5, 4, 3)
			>>> v1 = unit_vector(v0, axis=-1)
			>>> v2 = v0 / np.expand_dims(np.sqrt(np.sum(v0*v0, axis=2)), 2)
			>>> np.allclose(v1, v2)
			True
			>>> v1 = unit_vector(v0, axis=1)
			>>> v2 = v0 / np.expand_dims(np.sqrt(np.sum(v0*v0, axis=1)), 1)
			>>> np.allclose(v1, v2)
			True
			>>> v1 = np.empty((5, 4, 3))
			>>> unit_vector(v0, axis=1, out=v1)
			>>> np.allclose(v1, v2)
			True
			>>> list(unit_vector([]))
			[]
			>>> list(unit_vector([1]))
			[1.0]
		
			"""
			if out is None:
				data = np.array(data, dtype=np.float64, copy=True)
				if data.ndim == 1:
					data /= math.sqrt(np.dot(data, data))
					return data
			else:
				if out is not data:
					out[:] = np.array(data, copy=False)
				data = out
			length = np.atleast_1d(np.sum(data*data, axis))
			np.sqrt(length, length)
			if axis is not None:
				length = np.expand_dims(length, axis)
			data /= length
			if out is None:
				return data		
				
		def rotation_matrix(angle, direction, point=None):
			"""Return matrix to rotate about axis defined by point and direction.
		
			>>> R = rotation_matrix(math.pi/2, [0, 0, 1], [1, 0, 0])
			>>> np.allclose(np.dot(R, [0, 0, 0, 1]), [1, -1, 0, 1])
			True
			>>> angle = (random.random() - 0.5) * (2*math.pi)
			>>> direc = np.random.random(3) - 0.5
			>>> point = np.random.random(3) - 0.5
			>>> R0 = rotation_matrix(angle, direc, point)
			>>> R1 = rotation_matrix(angle-2*math.pi, direc, point)
			>>> is_same_transform(R0, R1)
			True
			>>> R0 = rotation_matrix(angle, direc, point)
			>>> R1 = rotation_matrix(-angle, -direc, point)
			>>> is_same_transform(R0, R1)
			True
			>>> I = np.identity(4, np.float64)
			>>> np.allclose(I, rotation_matrix(math.pi*2, direc))
			True
			>>> np.allclose(2, np.trace(rotation_matrix(math.pi/2,
			...                                               direc, point)))
			True
		
			"""
			sina = math.sin(angle)
			cosa = math.cos(angle)
			direction = unit_vector(direction[:3])
			# rotation matrix around unit vector
			R = np.diag([cosa, cosa, cosa])
			R += np.outer(direction, direction) * (1.0 - cosa)
			direction *= sina
			R += np.array([[ 0.0,         -direction[2],  direction[1]],
							[ direction[2], 0.0,          -direction[0]],
							[-direction[1], direction[0],  0.0]])
			M = np.identity(4)
			M[:3, :3] = R
			if point is not None:
				# rotation not around origin
				point = np.array(point[:3], dtype=np.float64, copy=False)
				M[:3, 3] = point - np.dot(R, point)
			return M
		
		rndAxis = np.random.rand(3)-0.5
		rndAngle = 2.*np.pi*np.random.rand(1)
		
		rndAxis/=np.linalg.norm(rndAxis)
		
		testlib.mat3_aangle.restype = mat3
		
		vax = vec3()
		vax.x = rndAxis[0]
		vax.y = rndAxis[1]
		vax.z = rndAxis[2]

		r = testlib.mat3_aangle(vax, ctypes.c_double(rndAngle[0]))
		
		R = rotation_matrix(rndAngle[0],rndAxis)
		
		
		np.testing.assert_almost_equal(r.a.x,R[0,0])
		np.testing.assert_almost_equal(r.a.y,R[0,1])
		np.testing.assert_almost_equal(r.a.z,R[0,2])
		np.testing.assert_almost_equal(r.b.x,R[1,0])
		np.testing.assert_almost_equal(r.b.y,R[1,1])
		np.testing.assert_almost_equal(r.b.z,R[1,2])
		np.testing.assert_almost_equal(r.c.x,R[2,0])
		np.testing.assert_almost_equal(r.c.y,R[2,1])
		np.testing.assert_almost_equal(r.c.z,R[2,2])
			
	def test_mat3_inv(self):
		
		
		rnd1 = np.matrix(np.random.rand(3,3))
		while (np.abs(np.linalg.det(rnd1))<1e-9):
			rnd1 = np.matrix(np.random.rand(3,3))
		
		m1 = mat3()
		
		m1.a.x = rnd1[0,0]; m1.a.y = rnd1[0,1]; m1.a.z = rnd1[0,2]
		m1.b.x = rnd1[1,0]; m1.b.y = rnd1[1,1]; m1.b.z = rnd1[1,2]
		m1.c.x = rnd1[2,0]; m1.c.y = rnd1[2,1]; m1.c.z = rnd1[2,2]
		
		
		testlib.mat3_inv.restype = mat3
		r = testlib.mat3_inv(m1)
		
		rndMInv = np.linalg.inv(rnd1)
		
		np.testing.assert_almost_equal(r.a.x,rndMInv[0,0])
		np.testing.assert_almost_equal(r.a.y,rndMInv[0,1])
		np.testing.assert_almost_equal(r.a.z,rndMInv[0,2])
		np.testing.assert_almost_equal(r.b.x,rndMInv[1,0])
		np.testing.assert_almost_equal(r.b.y,rndMInv[1,1])
		np.testing.assert_almost_equal(r.b.z,rndMInv[1,2])
		np.testing.assert_almost_equal(r.c.x,rndMInv[2,0])
		np.testing.assert_almost_equal(r.c.y,rndMInv[2,1])
		np.testing.assert_almost_equal(r.c.z,rndMInv[2,2])
		
		
		

if __name__ == '__main__':
    unittest.main()
