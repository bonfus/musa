MUon Site Analysis
==================

Requirements
------------

numpy           : numerical python, available on most linux distributions
pyspglib.spglib : library for finding lattice structure symmetries


Optional:

XCrysden

Usage
-----

The most confortable use is

ipython3 --autocall 2
