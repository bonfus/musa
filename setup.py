#!/usr/bin/env python

from distutils.core import setup, Extension

from distutils.command.install import INSTALL_SCHEMES
for scheme in INSTALL_SCHEMES.values():
    scheme['data'] = scheme['purelib']

## In case of missing numpy array
try:
    from numpy import get_include as numpy_get_include
    numpy_include_dir = [numpy_get_include()]
except:
    numpy_include_dir = []
    
setup(name='Musa',
      version='3.0a',
      description='MUon Site Analysis',
      author='Pietro Bonfa',
      author_email='pietro.bonfa@fis.unipr.it',
      url='https://www.no.url',
      packages=['musa','musa.test','musa.core','musa.modules', \
                'musa.modules.xsf','musa.modules.clfc','musa.modules.cif'],
      ext_modules=[Extension('lfcext', sources = ['musa/modules/clfc/ass.c', \
                                      'musa/modules/clfc/rass.c', \
                                      'musa/modules/clfc/incass.c', \
                                      'musa/modules/clfc/vec3.c', \
                                      'musa/modules/clfc/mat3.c', \
                                      'musa/modules/clfc/pile.c', \
                                      'musa/modules/clfc/dt.c',\
                                      'musa/modules/clfc/LFCExt.c'],
                                      libraries=['m'],
                                      include_dirs=numpy_include_dir,
                                      extra_compile_args=['-std=c99',],
                                      define_macros=[('_EXTENSION',None),])],
     package_dir={'musa': 'musa' },
     data_files=[('musa/core/', ['musa/core/spacegroup.dat',])],
     )
