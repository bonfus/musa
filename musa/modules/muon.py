from musa.core.parsers import *
from musa.core.nprint  import nprint, nprintmsg

import numpy as np

class Muon(object):
    def __init__(self):
        pass
    
        
        
    def muon_set_frac(self, arg = None):
        """
        Set muon position with respect to unit cell
        returns: None
        """
        if self._check_lattice():
            
            try:
                if arg is None:
                    pos=ninput('Muon position (frac coord): ', parse_vector)
                else:
                    if isinstance(arg, np.ndarray) and ( arg.shape == (3,)):
                        pos = arg
                    else:
                        pos = parse_vector(arg,3)
                        
            except EOFError:
                nprint("Ok.")
                return
            except TypeError:
                nprint("Cannot parse position.",'warn')
                return
                
            if (not isinstance(arg, np.ndarray)):
                pos = np.array(pos)
            
            self._muon.append(pos)
            return      
        else:
           nprintmsg('NS') 
            
            
    def muon_set_cart(self):
        nprint('Not implemented!','warn') 
        pass
        
        
    def muon_find_equiv(self, eps=1.e-3):
        """
        Given the unit cell symmetry, finds the equivalent muon sites.
        Magnetic order is NOT considered
        :params: eps, number of decimal figures for comparison
        """
        if self._check_sym() and self._check_muon():
            eqpoints = self._sym.equivalent_sites(np.array(self._muon), \
													symprec=eps, \
													onduplicates='warn')[0]
            self.muon_reset()
            for p in eqpoints:
                self._muon.append(np.array(p))
            
        else:
            nprintmsg('NS')
   
    def muon_reset(self):
        """
        Resets muon position.
        returns: None
        """
        self._muon = []
