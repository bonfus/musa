from musa.core.parsers import *
from musa.core.cells import get_reduced_bases
from musa.settings import config

import numpy as np

class DFTGrid():
    def __init__(self):
        self._uniform_grid = None
        self.tolerance = config.FCRD

        
    def dftgrid_build(self, size, min_distance_from_atoms=1.0):
        """
        Identifies symmetry operations of the unit cell
        """
        
        if self._check_lattice() and self._check_sym():
            
            # get symmmetry operations
            o = self._sym.get_symmetry_operations()
            # define rotations and translations
            r = o['rotations']
            t = o['translations']
            #
            #
                
            for mp in self._muon:
                for i in range(len(r)):
                    # new position for the muon
                    n = np.zeros(3)
                    # apply symmetry and bring back to unit cell
                    n = np.round(np.dot(r[i],mp)+t[i],decimals=self.tolerance)%1
                    
                    
                    for p in self._muon:
                        if np.allclose(n,p):
                            break
                    else:
                        self._muon.append(n)            
            
            
            
            #build uniform grid
            npoints = size**3
            x_ = np.linspace(0., 1., size, endpoint=False)
            y_ = np.linspace(0., 1., size, endpoint=False)
            z_ = np.linspace(0., 1., size, endpoint=False)

            uniform_grid = np.meshgrid(x_, y_, z_, indexing='ij')
            x,y,z = uniform_grid
            
            
            equiv=np.ones_like(x)*(npoints)
            
            for i in range(size):
                for j in range(size):
                    for k in range(size):
                        
                        if equiv[i,j,k] < npoints:
                            #this point is equivalent to someone else!
                            continue
                        
                        for ir in range(len(r)):
                            # new position for the muon
                            n = np.zeros(3)
                            # apply symmetry and bring back to unit cell
                            n = np.round(np.dot(r[ir],[x[i,j,k],y[i,j,k],z[i,j,k]])+t[ir],decimals=self.tolerance)%1
                            if (n*size - np.rint(n*size) < self.tolerance).all():
                                
                                #get index of point
                                ii,jj,kk=np.rint(n*size)
                                if (ii*(size**2)+jj*size+kk > i*(size**2)+j*size+k):
                                    equiv[ii,jj,kk] -= 1
                                    equiv[i,j,k] += 1
                                
            positions = []
            for i in range(size):
                for j in range(size):
                    for k in range(size):
                        if equiv[i,j,k] >= npoints:
                            #saves distances with all atoms
                            distances = []
                            center = [x[i,j,k],y[i,j,k],z[i,j,k]]
                            reduced_bases = self._cell.get_cell()
                            scaled_pos = self._cell.get_scaled_positions()
                            for a in range(len(self._cell)):
                                
                                for ii in (-1, 0, 1):
                                    for jj in (-1, 0, 1):
                                        for kk in (-1, 0, 1):
                                            distances.append( np.linalg.norm(
                                                    np.dot(scaled_pos[a] - center + np.array([ii,jj,kk]),
                                                        reduced_bases) ) )
                                                        
                            if min(distances) > min_distance_from_atoms:
                                positions.append([x[i,j,k],y[i,j,k],z[i,j,k]])
                                
            return positions
        return None

                                                   
                
            
            
        

        
