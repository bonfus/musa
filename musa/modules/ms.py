# http://magcryst.org/resources/magnetic-coordinates/

from musa.core.magmodel import MM

from musa.core.parsers import *
from musa.core.nprint  import nprint, nprintmsg, print_cell
from musa.core.ninput  import *

from musa.modules.cif.cif import parse_cif,parse_magn_operation_xyz_string
from musa.modules.cif.cell import cellpar_to_cell


import numpy as np


class MS(object):
    # magnetic structure selector
    
    def __init__(self):
        self._selected_order = -1
    
        
            
    def mago_select(self,arg):
        """
        Select magnetic strtures among the already defined
        """
        i=0
        try:
            i = int(arg)
        except:
            nprintmsg('ema')
            nprintmsg('NS')
        
        if arg>len(self._magdefs) or arg<1:
            nprintmsg('ema')
            nprintmsg('NS')
        
        self._selected_order=i-1
        
    
    def _fourier_components(self):
        if self._check_magdefs:
            return self._magdefs[self._selected_order].fc
        else:
            raise RuntimeError        
            
    def _k(self):
        if self._check_magdefs:
            return self._magdefs[self._selected_order].k
        else:
            raise RuntimeError        
        
    def _phi(self):
        if self._check_magdefs:
            return self._magdefs[self._selected_order].phi
        else:
            raise RuntimeError     

    def mago_set_k(self,kvalue=None,mm=None):
        """
        Set propagation with respect to unit cell
        kavalue : propagation vector (either string or tuple or list), if None, input is prompetd
        mm      : Magnetic Model to be used. If None the current magnetic model is used
        returns: None
        """
        if mm is None:
            if self._selected_order >= 0:
                smm = self._magdefs[self._selected_order]
            else:
                nprintmsg('emag')
                return
        else:
            smm = mm
            
            
        
        if self._check_lattice():
            if not kvalue is None:
                if isinstance(kvalue, np.ndarray) and ( kvalue.shape == (3,)):
                    smm.k = kvalue
                    return
            else:
                try:
                    if kvalue is None:
                        kval=ninput('Propagation vector (w.r.t. unit cell): ', parse_vector)
                    else:
                        kval = parse_vector(kvalue,3)
    
                except EOFError:
                    nprint("Ok.")
                    return
                except TypeError:
                    nprint("Cannot parse position.",'warn')
                    return
                smm.k=np.array(kval,dtype=np.float)
                return
        else:
           nprintmsg('NS') 


    def mago_add(self, arg=None):
        """
        Add a magnetic model (fourier components and K vector).
        Fourier components are in CARTESIAN coordinates.
        The order is automatically selected if succesfully added.
        
        arg: type: MM class. if present, add the class.
        
        returns: None
        """        
        # we nead at least a crystal lattice!
        if not self._check_lattice():
            nprintmsg('NS')
            return
            
        if isinstance(arg, MM):
            
            # TODO!!
            # check if it has the same number of fourier components of the crystal
            # check if it has the same number of fourier components of the crystal
            #
            self._magdefs.append(arg) 
            self._selected_order=len(self._magdefs)-1
            return
        
        
        nmm = MM(self._cell.get_number_of_atoms())
        
        self.mago_set_k(mm=nmm)
        self.mago_set_FC(mm=nmm)
        self._magdefs.append(nmm)
        self._selected_order=len(self._magdefs)-1
        

            

    def mago_set_FC(self, fcs= None, atoms_types = None, mm=None):
        """
        defines fourier components for the unit cell.
        Values are given in CARTESIAN coordinates (this is different from mcif convention)
        """
        if mm is None:
            if self._selected_order >= 0:
                smm = self._magdefs[self._selected_order]
            else:
                nprintmsg('emag')
                return
            
        else:
            smm = mm
        
        
        
        if self._check_lattice():
            
            
            unit_cell = self._cell
            
            if not fcs is None:
                if not isinstance(fcs, np.ndarray):
                    nprint('Wrong number of Fourier Components!')
                if len(fcs) != unit_cell.get_number_of_atoms():
                    nprint('Wrong number of Fourier Components!')
                smm.fc = fcs
                return
                    
            
            
            
            
            if atoms_types is None:
                atoms_types=ninput('Which atom? (enter for all)').split()
            
            if atoms_types == []:
                set_this_fc = lambda x: True
            else:
                atoms_types = [x.lower() for x in atoms_types] #lowercase
                set_this_fc = lambda x: x.lower() in atoms_types
                # Print where they are
                print_cell(unit_cell,set_this_fc)

            nprint('Magnetic moments in cartesian coordinates.')
            for i, atom in enumerate(unit_cell):
                
                try:
                    if set_this_fc(atom[0]): # check if we should set this fc
                        FCin = ninput_mt("FC for atom %i %s (3 real, [3 imag]): " % (i+1,atom[0]), parse_complex_vector)
                                

                        smm.fc[i]=[FCin[i]+1.j*FCin[i+3] for i in range(3)]
                    else:
                        smm.fc[i]=([0.+0.j,0.+0.j,0.+0.j])
                        
                except EOFError:
                    break 


        else:
           nprintmsg('NS') 
        
    def mago_load_from_mcif(self, fname):
        #http://www.physics.byu.edu/faculty/campbell/events/cmsworkshop2014/campbell_magCIF.pdf
        
        floateps = np.finfo(np.float).eps*10
        
        if self._check_lattice():
            try:
                f = open(fname,'r')
            except:
                nprintmsg('efile')
                return
            
            data = parse_cif(f)
            
            f.close()
            
            nprint('WARNING! This function is experimental! Do not use for production!','warn')
            
            nprint('Parsing data from: ' + data[0][0])
            
            values = data[0][1]
            
            # find magnetic cell
            
            a = values['_cell_length_a']
            b = values['_cell_length_b']
            c = values['_cell_length_c']
            alpha = values['_cell_angle_alpha']
            beta = values['_cell_angle_beta']
            gamma = values['_cell_angle_gamma']
            
            mag_cell = cellpar_to_cell([a, b, c, alpha, beta, gamma], (0,0,1), None)

            
            # Find magnetic atoms
            
            mag_atoms_labels = values['_atom_site_moment_label']

            # load mag moments
            mag_atoms_moments = np.zeros([len(mag_atoms_labels),3],dtype=np.float)
            
            for i in range(len(mag_atoms_labels)):
                
                mag_atoms_moments[i,0] = values['_atom_site_moment_crystalaxis_x'][i]
                mag_atoms_moments[i,1] = values['_atom_site_moment_crystalaxis_y'][i]
                mag_atoms_moments[i,2] = values['_atom_site_moment_crystalaxis_z'][i]
                
            # THESE ARE IN CRYSTAL AXIS COORDINATE SYSTEM!!!
            # bohr magneton units are used
            # the magnetic metric tensor is M = L.G.L^(-1), which is unitless. 
            # NOW GO TO REDUCED LATTICE COORDINATE SYSTEM TO DO THE SYMMETRY
            L = np.diag([1./a,1./b,1./c])    
            mag_atoms_moments = np.dot(mag_atoms_moments,L)
            
            mag_atoms_pos = np.zeros([len(mag_atoms_labels),3])
            mag_atoms_sym = []
            i = 0
            for label in mag_atoms_labels:
                j = values['_atom_site_label'].index(label)
                mag_atoms_pos[i,0] = values['_atom_site_fract_x'][j]
                mag_atoms_pos[i,1] = values['_atom_site_fract_y'][j]
                mag_atoms_pos[i,2] = values['_atom_site_fract_z'][j]
                mag_atoms_sym.append(values['_atom_site_type_symbol'][j])
                i += 1
            
            
            # Find transformation matrix between the two cells
            #  OCell = A * MCell
            
            unit_cell = self._cell   
            fcs = np.zeros([unit_cell.get_number_of_atoms(),3],dtype=np.complex)
            
            atoms_types = [x.lower() for x in mag_atoms_sym] #lowercase
            set_this_fc = lambda x: x.lower() in atoms_types
            
            # count magnetic atoms and add their positions
            cart_pos = unit_cell.get_positions()
            ocell_cart_pos = []
            ocell_crys_pos = []
            for i, atom in enumerate(unit_cell):
                if set_this_fc(atom[0]): # check if we should set this fc
                    ocell_cart_pos.append(cart_pos[i])
                    ocell_crys_pos.append(atom[2])

            
            # get propagation vector
            k = np.fromstring(values['_magnetic_propagation_vector_kxkykz'][0],dtype=float, sep=',')
            
            # define coefficients for linear system
            ac = [[] for x in range(len(ocell_crys_pos))]
            bc = [[] for x in range(len(ocell_crys_pos))]
            
            
            for j, m_a_p in enumerate(mag_atoms_pos):
                for cent in values['_space_group_symop.magn_centering_xyz']:
                    rc,tc,trc = parse_magn_operation_xyz_string(cent)
                    cm_a_p = (np.dot(rc,m_a_p)+tc)
                    
                    for s in values['_space_group_symop.magn_operation_xyz']:
                        
                        r,t,tr = parse_magn_operation_xyz_string(s)
                        
                        symp = (np.dot(r,cm_a_p)+t)
                        
                        #print('Symp is: '+ str(symp))
                        
                        mag_atom_cart_pos = np.dot(symp, mag_cell)
                        # clean noise
                        mag_atom_cart_pos[np.abs(mag_atom_cart_pos)< floateps] = 0.
                        
                        mag_atom_crys_pos = np.dot(mag_atom_cart_pos,
                                        np.linalg.inv(unit_cell.get_cell()))
                        # clean noise
                        mag_atom_crys_pos[np.abs(mag_atom_crys_pos)< floateps] = 0.                                        

                        #print('trc,np.linalg.det(rc),tr,np.linalg.det(r),np.dot(r, np.dot(rc,mag_atoms_moments[j])), np.dot(rc,mag_atoms_moments[j])')
                        
                        #print(trc,np.linalg.det(rc),tr,np.linalg.det(r),np.dot(r, np.dot(rc,mag_atoms_moments[j])), np.dot(rc,mag_atoms_moments[j]))
                        
                        #print('r')
                        #print(r)
                        
                        crysfc = trc*np.linalg.det(rc)*tr*np.linalg.det(r)*np.dot(r, np.dot(rc,mag_atoms_moments[j]))
                        
                        # Go to cartesian coordinates.
                        cart_fcs = np.dot(crysfc,mag_cell)  ##### THIS IS WRONG!!
                        
                        for i, p in enumerate(ocell_crys_pos):
                            
                            spos = np.remainder(np.round(mag_atom_crys_pos,decimals=4),1.)


                            if np.allclose( spos , p, rtol=1e-03):
                                val = mag_atom_crys_pos - spos
                                cs = np.cos(2.*np.pi*np.dot(k,val))
                                sn = np.sin(2.*np.pi*np.dot(k,val))
                                
                                ac[i].append( [cs,sn])
                                bc[i].append( cart_fcs)
                                break
            
            
            j = 0
            for i, atom in enumerate(unit_cell):
                if set_this_fc(atom[0]): # check if we should set this fc
                    minsqx = np.linalg.lstsq(np.array(ac[j]),np.array(bc[j])[:,0],rcond=1e-7)
                    minsqy = np.linalg.lstsq(np.array(ac[j]),np.array(bc[j])[:,1],rcond=1e-7)
                    minsqz = np.linalg.lstsq(np.array(ac[j]),np.array(bc[j])[:,2],rcond=1e-7)
                    
                    
                    xre,xim = np.round(minsqx[0],decimals=6)
                    yre,yim = np.round(minsqy[0],decimals=6)
                    zre,zim = np.round(minsqz[0],decimals=6)
                    fcs[i]=[np.complex(xre,xim),np.complex(yre,yim),np.complex(zre,zim)]
                    #print ('fcs[%d]'%i)
                    #print (fcs[i])
                    j += 1
            

            nmm = MM(unit_cell.get_number_of_atoms())
            nmm.k = k
            nmm.fc = fcs
            
            
            self.mago_add(nmm)


    def _find_coefficients(self,ac,bc):
        # try without phase
        minsqx = np.linalg.lstsq(np.array(ac[j]),np.array(bc[j])[:,0],rcond=1e-7)
        minsqy = np.linalg.lstsq(np.array(ac[j]),np.array(bc[j])[:,1],rcond=1e-7)
        minsqz = np.linalg.lstsq(np.array(ac[j]),np.array(bc[j])[:,2],rcond=1e-7)
        
        if (minsqx[1] < 1e-10 and minsqy[1] < 1e-10 and minsqz[1] < 1e-10):
        
            xre,xim = np.round(minsqx[0],decimals=6)
            yre,yim = np.round(minsqy[0],decimals=6)
            zre,zim = np.round(minsqz[0],decimals=6)
            
        else:
            raise RuntimeError
            
            
        return [np.complex(xre,xim),np.complex(yre,yim),np.complex(zre,zim)]        
    
